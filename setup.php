<?php

$this->executeSQL("
CREATE TABLE IF NOT EXISTS `#prefix#banklink_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bank` varchar(20) NOT NULL,
  `payment_id` int(11) NOT NULL,
  `system_payment_id` varchar(50) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '0',
  `content` text NOT NULL,
  `session` varchar(20),
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
");

$this->executeSQL("
CREATE TABLE IF NOT EXISTS `#prefix#banklink_payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `system_id` varchar(255) NOT NULL,
  `creator_id` int(11) NOT NULL,
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `amount` float NOT NULL,
  `ip` varchar(16) NOT NULL,
  `lang` varchar(10) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `semistatus` int(11) NOT NULL DEFAULT '0',
  `transaction_id` varchar(50) NOT NULL,
  `bank` varchar(10) NOT NULL,
  `crc` text NOT NULL,
  `response` text,
  `response_time` datetime DEFAULT NULL,
  `description` text NOT NULL,
  `errors` text NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1
");