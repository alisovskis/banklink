<?php
include_once __DIR__ . "/Bank.php";
include_once __DIR__ . "/Error.php";
require_once __DIR__ . '/XML/xmlseclibs.php';
require_once __DIR__ . '/XML/Serializer.php';
require_once __DIR__ . '/XML/Unserializer.php';

/*
 * payment statuss
 *
 * 0 - new payment
 * 1 - success, confirmed
 *
 * 2 - failed
 * 3 - declined
 * 4 - reversed
 * 5 - autoreversed
 * 6 - timeouted
 * 7 - canceled
 * 10 - signed
 * 11 - processing
 *
 * 12 - card disabled
 * 13 - invalid transaction
 * 14 - card invalid
 * 15 - insufficient funds
 * 16 - response statuss N
 *
 * 99 - technical error
 * 100 - reversed from admin
 * 999 - timeouted
 */

class Banklink{

    static public $configFile = false;

    public $dir = false;
    private $config = [];
    public $errorMessage = false;

    /*
     * => constructor
     *
     * => params
     *
     * @framework - string representing framework [default - codeigniter)
     *
     */

    private function getLastError($item) {
        $this->errorMessage = $item->errorMessage;
    }

    public function __construct($framework = false, $configFile = false) {
        $this->dir = __DIR__;

        include $this->dir . '/config.php';

        $this->config = $config;

        if ($config) {
            $this->importConfig($configFile);
        }


        if (!$framework) {
            $framework = $this->config['framework'];
        } else {
            $this->config['framework'] = $framework;
        }

        include_once $this->dir . '/db/' . $this->config['framework'] . ".php";

        $this->db = new BanklinkDb();
        $this->e = new Error($this);
    }

    public function importConfig($file) {
        if (file_exists($file)) {

            include $file;

            Banklink::$configFile = $file;

            if (isset($config)) {
                foreach ($config as $k => $c) {
                    if ($k!='libraries') {
                        if (isset($this->config[$k])) {
                            $this->config[$k] = $c;
                        }
                    } else {
                        foreach ($c as $bankKey => $bank) {

                            if (isset($this->config['libraries'][$bankKey])) {

                                    foreach ($bank as $key => $option) {
                                        if ($key!='test') {
                                            $this->config['libraries'][$bankKey][$key] = $option;
                                        } else {
                                            foreach ($option as $tkey => $toption) {
                                                $this->config['libraries'][$bankKey]['test'][$tkey] = $toption;
                                            }
                                        }
                                    }
                                }

                        }
                    }
                }
            } else {
                echo 'Config file empty!';
            }


        } else {
            echo 'Config file not Found!';
        }
    }

    /*
     *  => set base config (not library settings)
     *
     *  => params
     *
     *  @settings - array (key=>value) pairs to replace default config
     *
     */

    public function setBase($settings) {
        foreach ($settings as $ky=>$val) {
            if ($ky!=='libraries') {
                $this->config[$ky] = $val;
            }
        }
    }

    /*
     *  => set individual banks config
     *
     *  => params
     *
     *  @bank - bank id string
     *  @settings - array (key=>value) pairs to replace default config
     *
     */

    public function setBank($bank, $settings) {

        if (isset( $this->config['libraries'][$bank])) {
            foreach ($this->config['libraries'][$bank] as $ky => $val) {
                if (isset($settings[$ky])) {
                    $this->config['libraries'][$bank][$ky] = $settings[$ky];
                }
            }
        }

    }

    /*
     * => request for banklink
     *
     * => params
     *
     * $bank - bank ID (string)
     * $settings - array
     *      @id - buys unique ID
     *      @amount - ammount of money
     *      @description - description of payment to display in bank details
     *      @lang - bank interface language.
     *
     * => return
     *
     * @url - url to call
     * @post - data to send with post [may be empty)
     * @get - data to send with get (may be empty)
     * @error - if false - success; if true - see @errorMessage
     * [@errorMessage] - if failed - returns received error message.
     *
     */

    public function buy($bank, $settings) {

        $this->e->log('base',array('content'=>array('bank'=>$bank, 'settings'=>$settings), 'description'=>'Starting Buy sequence'));
        if (isset($this->config['libraries'][$bank]) && $this->config['libraries'][$bank]['enabled']) {
            include_once($this->dir . '/apis/' . $bank . '/index.php');
            $bankI = new $bank($this, $this->config['libraries'][$bank]);

            if (!isset($bankI->settings['responseUrl'])) {
                $bankI->settings['responseUrl'] = str_replace('#bank#', $bank, $this->config['responseUrl']);
            }
            if (!isset($bankI->settings['failUrl'])) {
                $bankI->settings['failUrl'] = str_replace('#bank#', $bank, $this->config['failUrl']);
            }
            if (!isset($bankI->settings['cancelUrl'])) {
                $bankI->settings['cancelUrl'] = str_replace('#bank#', $bank, $this->config['cancelUrl']);
            }

            $result = $bankI->buy($settings);

            if (isset($result['error']) && $result['error']==true) {
                $this->getLastError($bankI);
                $result['errorMessage'] = $this->errorMessage;

                if ($bankI->payment_id) {
                    $bankI->saveErrors($bankI->payment_id, $result['errorMessage']);
                }
            }

            return $result;
        } else {
            $this->e->error('base',array('content'=>array('bank'=>$bank, 'settings'=>$settings), 'description'=>'bank_not_enabled'));
            $result['error']=true;
            $result['errorMessage'] = $this->errorMessage;
            return $result;
        }

    }

    /*
     * => confirm buy
     *
     * => params
     *
     * @bank - bank identifier string
     * -----@settings - array of request settings if any.
     *
     * => returns
     * array
     *      @id - payment id given by system before.
     *      @error - if false - success; if true - see @errorMessage
     *      [@errorMessage] - if failed - returns received error message.
     *
     * if error = >false and id is not false - payment was success
     *
     */
    public function confirm($bank, $forceData = false) {
        $this->e->log('base',array('content'=>array('bank'=>$bank), 'description'=>'Starting Confirm sequence'));
        if (isset($this->config['libraries'][$bank])) {
            include_once($this->dir . '/apis/' . $bank . '/index.php');
            $bankI = new $bank($this, $this->config['libraries'][$bank]);

            $result = $bankI->confirm($forceData);

            if (isset($result['error']) && $result['error']==true) {
                $this->getLastError($bankI);
                $result['errorMessage'] = $this->errorMessage;

                if ($bankI->payment_id) {
                    $bankI->saveErrors($bankI->payment_id, $result['errorMessage']);
                }
            }

            $result['id'] = $bankI->payment_system_id;
            $result['status'] = $this->status($bankI->payment_system_id);
            $result['status'] = $result['status']['status'];
            $result['fromServer'] = $bankI->fromServer;

            if (!isset($result['wait'])) {
                $result['wait'] = false;
            }

            return $result;

        }

    }

    public function error($bank) {
        $this->e->log('base',array('content'=>array('bank'=>$bank), 'description'=>'Starting Error sequence'));
        if (isset($this->config['libraries'][$bank])) {
            include_once($this->dir . '/apis/' . $bank . '/index.php');
            $bankI = new $bank($this, $this->config['libraries'][$bank]);

            $result = $bankI->error();

            if (isset($result['error']) && $result['error']==true) {
                $this->getLastError($bankI);
                $result['errorMessage'] = $this->errorMessage;

                if ($bankI->payment_id) {
                    $bankI->saveErrors($bankI->payment_id, $result['errorMessage']);
                }
            }

            $result['id'] = $bankI->payment_system_id;
            $result['status'] = $this->status($bankI->payment_system_id);
            $result['status'] = $result['status']['status'];
            $result['fromServer'] = $bankI->fromServer;

            return $result;
        }

    }

    protected function requireResponse($bank) {
        if (isset($bank['bank'])) {
            $bankName = strtolower($bank['bank']);
        } else {
            return false;
        }

        include_once($this->dir . '/apis/' . $bankName . '/index.php');
        $bankI = new $bankName($this, $this->config['libraries'][strtolower($bankName)]);
        $done = $bankI->requireStatus($bank['system_id']);
        return $done;
    }

    public function cancel($bank) {
        $this->e->log('base',array('content'=>array('bank'=>$bank), 'description'=>'Starting Cancel sequence'));
        if (isset($this->config['libraries'][$bank])) {
            include_once($this->dir . '/apis/' . $bank . '/index.php');
            $bankI = new $bank($this, $this->config['libraries'][$bank]);

            $result = $bankI->canceled();

            if (isset($result['error']) && $result['error']==true) {
                $this->getLastError($bankI);
                $result['errorMessage'] = $this->errorMessage;

                if ($bankI->payment_id) {
                    $bankI->saveErrors($bankI->payment_id, $result['errorMessage']);
                }
            }

            $result['id'] = $bankI->payment_system_id;
            $result['status'] = $this->status($bankI->payment_system_id);
            $result['status'] = $result['status']['status'];
            $result['fromServer'] = $bankI->fromServer;

            return $result;

        }
    }

    public function cron() {
        $this->e->log('base',array('content'=>array(), 'description'=>'Starting Cron sequence'));
        $response = [];
        foreach ($this->config['libraries'] as $ky => $val) {
            if ($val['enabled']) {
                include_once($this->dir . '/apis/' . $ky . '/index.php');
                $bankI = new $ky($this, $val);
                $response[$ky] = $bankI->cron();

                if (isset($response[$ky]['error']) && $response[$ky]['error'] == true) {
                    $response[$ky]['errorMessage'] = $bankI->errorMessage;
                }
            }
        }

        return $response;
    }

    public function status ($system_id, $dataReady = false) {
        $this->e->log('base',array('content'=>array('data'=>$system_id, 'way'=>$dataReady), 'description'=>'Starting Status sequence'));

        if (!$dataReady) {
            $data = $this->db->execute("SELECT * FROM `" . str_replace('#prefix#', $this->config['tablePrefix'], $this->config['table']) . "` WHERE `system_id`=" . $this->db->escape($system_id));
            $data = $data[0];
        } else {
            $data = $system_id;
        }

        //var_dump($data);

        if (!empty($data)) {
            $status = array('error'=>false, 'fromServer'=>-1, 'id'=>$data['system_id'], 'status'=>$data['status'], 'wait'=>false);

            if ($status['status']==0) {
                $re = $this->requireResponse($data);
                if ($re!=false) {
                    $status = $re;
                }
            }
        } else {
            $this->e->error($this, array('content' => array('request' => $data), 'description' => 'payment_not_found'));
            $status = array('error'=>true, 'fromServer'=>-1, 'id'=>$data['system_id'], 'errorMessage' => $this->errorMessage, 'wait'=>false);
        }

        return $status;
    }

    public function requestStatus () {
        $this->e->log('base',array('content'=>array(), 'description'=>'Starting Status Request sequence'));
        $data = $this->db->execute("SELECT * FROM `". str_replace('#prefix#', $this->config['tablePrefix'], $this->config['table']) . "` WHERE `status`=0 AND `created` between DATE_SUB(NOW(), INTERVAL " . intval($this->getConfig('failedRequest')) . " MINUTE) AND DATE_SUB(NOW(), INTERVAL " . intval($this->getConfig('startForcedRequests')) . " MINUTE)");

        $result = array();
        foreach ($data as $out) {
            $d = $this->status($out, true);
            if ($d['status']>0) {
                $result[] = $d['status'];
            }
        }

        return $result;
    }

    /*
     *  get base config item
     *
     *  @item - key title to retrieve
     *
     */

    public function getConfig($item) {
        if (isset($this->config[$item])) {
            return $this->config[$item];
        }
    }

    /*
     * makes default tables to work.
     */

    public function setup() {
        foreach ($this->config['libraries'] as $ky => $val) {
            if ($val['enabled']) {
                if (file_exists($this->dir . '/apis/' . $ky . '/setup.php')) {
                    include_once($this->dir . '/apis/' . $ky . '/setup.php');
                }
            }
        }

        if (file_exists($this->dir . '/setup.php')) {
            include_once($this->dir . '/setup.php');
        }
    }

    /*
     * calls plain sql execution for required framework.
     */

    public function executeSQL($sql) {
        $sql = str_replace('#prefix#', $this->config['tablePrefix'], $sql);
        return $this->db->execute($sql);
    }

    /*
    * calls plain sql execution for required framework.
    */

    public function escape($text) {
        return $this->db->escape($text);
    }

    static public function getData($type, $param) {
        if ($type=='db') {
            return BanklinkDb::get($param);
        }

        if ($type=='global') {

            include __DIR__ . '/config.php';

            if ($param!='libraries' && isset($config[$param])) {
                return $config[$param];
            }
        } else {

            include_once __DIR__ . '/config.php';

            if (isset($config['libraries'][$type])) {

                if (isset($config['libraries'][$type][$param])) {
                    return $config['libraries'][$type][$param];
                }

            }

        }

        return false;
    }

    // TO BE implemented

    public function auth($bank){} //authenticate without any money withdrawal

    public function resend($bank, $system_id) {} //resend request (can be done if status is 0. Like - if connection for user crashes.

    public function getList($bank=false, $status=false, $page = 1, $entries = 100) {

        $this->e->log('base',array('content'=>array($bank), 'description'=>'Starting GetList sequence'));

        $query = '';

        if ($bank && isset($this->config['libraries'][$bank]) && $this->config['libraries'][$bank]['enabled']) {
            $query .= " AND `bank`='" . ucfirst($bank) . "'";
        }

        if ($status!==false) {
            $query .=" AND `status`=" . intval($status);
        }

        $limit = (intval($page)-1)*$entries;

        $return = $this->db->execute("SELECT system_id, creator_id, created, amount, IP, status, bank, transaction_id, description FROM " . str_replace('#prefix#', $this->config['tablePrefix'], $this->config['table']) . " WHERE 1=1 " . $query . " LIMIT " . $limit . ", " . intval($entries));

        return $return;

    }

    public  function reverse($system_id, $bank = false) {

        $this->e->log('base',array('content'=>array($bank), 'description'=>'Starting Reverse sequence'));

        if (isset($this->config['libraries'][$bank]) && $this->config['libraries'][$bank]['enabled']) {
            include_once($this->dir . '/apis/' . $bank . '/index.php');
            $bankI = new $bank($this, $this->config['libraries'][$bank]);

            $done = $bankI->reverse($system_id);
        } else {
            $bank = $this->db->execute("SELECT * FROM " . str_replace('#prefix#', $this->config['tablePrefix'], $this->config['table']) . " WHERE system_id='" . $system_id . "'");

            if (isset($bank[0]['bank'])) {
                $bank = strtolower($bank[0]['bank']);
            } else {
                return false;
            }

            include_once($this->dir . '/apis/' . $bank . '/index.php');
            $bankI = new $bank($this, $this->config['libraries'][strtolower($bank)]);

            $done = $bankI->reverse($system_id);
        }

        if (isset($done['error']) && $done['error']==true) {
            $this->getLastError($bankI);
            $done['errorMessage'] = $this->errorMessage;
        }

        return $done;
    }

}