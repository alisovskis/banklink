<?php

class Error {

    public $p = false;
    private $sessionId = false;

    public function __construct($p) {
        $this-> sessionId = time() . rand(1000, 9999) . rand (1000, 9999);
        $this->p = $p;
    }

    public function log($bank, $data) {

        if ($this->verify($data)) {
            if ($this->p->getConfig('log')==true) {
                $this->createInsert($bank, $data, $this->p->getConfig('log_table'), 1);
                if (is_object($bank)) {
                    $bank->log[] = $data['description'];
                }
            }
        }

    }

    private function createInsert($bank, $data, $table, $type = 0) {

        if (!is_string($bank)) {
            $array = [
                'content' => json_encode($data['content']),
                'system_payment_id' => $bank->payment_system_id?$bank->payment_system_id:'0',
                'payment_id' => $bank->payment_id?$bank->payment_id:'0',
                'type' => $type,
                'bank' => $bank->id,
                'session'=> $this->sessionId,
                'description' => $data['description']
            ];
        } else {
            $array = [
                'content' => json_encode($data['content']),
                'type' => $type,
                'bank' => 'main',
                'session'=> $this->sessionId,
                'description' => $data['description']
            ];
        }

        $this->p->db->insert($table, $array);
    }

    public function debug($bank, $data) {

        if ($this->verify($data)) {
            if ($this->p->getConfig('debug')==true) {
                $this->createInsert($bank, $data, $this->p->getConfig('debug_table'), 2);
            }
        }

    }

    public function error($bank, $data) {

        if ($this->verify($data)) {

            $data['description'] = $this->code($data['description']);

            if (!is_string($bank)) {
                if (!isset($data['description'])) {
                    if (count($bank->errorMessage)>0) {
                        $data['description'] = $bank->errorMessage[key(array_slice($bank->errorMessage, -1, 1, TRUE))];
                    } else {
                        $data['description'] = 'Unknown';
                    }
                } else {
                    $data['description']['params'] = $data['content'];
                    $bank->errorMessage[] = $data['description'];
                }

                $bank->error = true;
            } else {
                $data['description']['params'] = $data['content'];
                $this->p->errorMessage[] = $data['description'];
            }

            $data['description'] = 'Errno: ' . $data['description']['errno'] . " -> " . $data['description']['text'];

            $this->createInsert($bank, $data, $this->p->getConfig('error_table'), 3);
        }

    }

    public function verify($data) {
        return true;
        if (!isset($data['id'])) {
            return false;
        }

        if (!isset($data['type'])) {
            return false;
        }
    }

    function errorCodes () {
        return [
            'status_already_set'=>[1, 'Status already set'], //Status is set, wher trying to update
            'technical_diff'=> [2,'Technical difficulties'],
            'cancelled_transaction'=>[3,'Called response cancelled'],
            'status_already_confirmed'=>[4, 'Status already confirmed'],  //Status is set, when trying to update "status = 1"
            'missing_status'=>[5, 'Missing payment status'],
            'payment_not_accepted'=>[6, 'Payment wasnt accepted, money wasn\'t transfered'],
            'payment_not_found'=>[7, 'Could not get Payment data'],
            'file_not_found'=>[8, 'File not found'],
            'config_file_empty'=>[9, 'Config file you try to import is empty'],
            'bank_not_enabled'=>[10,'Invalid or bank is not enabled'],
            'seb_error_generating_crc'=>[11, 'Error while generating CRC'],
            'private_key_not_found'=>[12, 'Private Key file not found'],
            'system_id_already_in_use'=>[13,'System id already in use'],
            'fd_could_not_create_payment'=>[14, 'Could not create new payment'],
            'fd_could_not_create_transaction'=>[15, 'Could not create new transaction'],
            'could_not_verify_payment'=>[16, 'Could not verify payment'],
            'fd_payment_too_old'=>[17,'Entry older than 72 hours! Can\'t reverse'],
            'fd_unidentified_error'=>[18,'Unidentifief problem! Can\'t reverse'],
            'fd_status_is_not_confirmed'=>[19,'Status is not "confirmed"!'],
            'fd_auth_failed'=>[20, 'Auth failed.'],
            'could_not_update'=>[21, 'Could not update data'],
            'missing_transaction_id'=>[22, 'No Transaction Id supplied'],
            'failed_day_closure'=>[23, 'Could not finish workday'],
            'could_not_validate'=>[24, 'Could not validate data'],
            'could_not_generate_mac'=>[25, 'Could not generate MAC for request'],
            'private_key_not_set'=>[26, 'Private Key file not set'],
            'nv_mac_calculation_failed'=>[27, 'Mac calculation failed!'],
            'nv_mac_was_incorect'=>[28,'Mac was incorrect!'],
            'seb_signature_is_invalid'=>[29,'Signature is invalid'],
            'seb_error_with_signature'=>[30, 'Error while checking signature'],
            'seb_error_with_certificate'=>[31, 'Error while checking certificate'],
            'sw_private_key_couldnt_open'=>[32, 'Opening SSL private key, Failed'],
            'sw_could_not_generate_payment'=>[33,'Could not create new payment'],
            'sw_reading_public_key_failed'=>[34, 'Reading public key, Failed'],
            'sw_payment_unaccepted'=>[35, 'Payment wasn\'t accepted, money wasn\'t transfered'],
            'sw_no_confirmation_status'=>[36, 'No confirmation status'],
            'sw_signature_invalid'=>[37, 'Signature is invalid'],
            'sw_status_reveived_fail'=>[38, 'Received Canceled or other fail status from bank'],
            'sw_errors_updating_payment'=>[39, 'Error while updating payment'],
            'payment_status_already_set'=>[40, 'Payment status already set'],
            'signature_creation_error'=>[41, 'Signature creation error'],
            'doesnt_have_any_identifier'=>[42, 'Does not have any identifier'],
            'declined_transaction'=>[43, 'Transaction was declined'],
            'fd_transaction_not_found'=>[44, 'Transaction not found'],
            'seb_service_info_not_found'=>[45, 'Service info not found'],
            'seb_signature_invalid'=>[46, 'Signature invalid'],
            'payment_not_found'=>[47, 'Payment not found']
        ];
    }

    function code($key) {
        $me = $this->errorCodes();

        if (isset($me[$key])) {
            return ['text'=>$me[$key][1], 'errno'=>$me[$key][0]];
        } else {
            return ['text'=>$key, 'errno'=>9999];
        }
    }

}