<?php

abstract class Bank {
    public $settings = false;
    public $options = array();
    protected $p = false;
    public $error = false;
    public $errorMessage = array();
    public $log = array();
    public $id = '';
    public $fromServer = -1;

    public $payment_transaction_id = false;
    public $payment_id = false;
    public $payment_system_id = false;

    public function __construct($parent, $settings)  {
        $parent->e->log($this->id,array('content'=>array('settings'=>$settings), 'description'=>'Bank init done'));
        $this->p = $parent;
        $this->settings = $settings;

        if (!$this->p->getConfig('production')) {
            if (isset($this->settings['test'])) {
                foreach ($this->settings['test'] as $k => $v) {
                    $this->settings[$k] = $v;
                }
            }
        }

    }

    public function xmlToArray($response){
        $unserializer = new XML_Unserializer();
        $status = $unserializer->unserialize($response);
        error_reporting(E_ERROR);
        if(PEAR::isError($status)){
            error_reporting(E_ALL);
            //$status->getMessage(); //error message
            return false;
        }
        error_reporting(E_ALL);
        return $unserializer->getUnserializedData();
    }

    abstract public function buy($data);

    public function reverse($id) {
        return false;
    }

    public function requireStatus($system_id) {
        return false;
    }

    public function bySystemId($id) {
        $this->p->e->log($this,array('content'=>array('id'=>$id), 'description'=>'Get payment details (by id).'));
        if (empty($id)) return false;
        $result = $this->p->executeSQL("SELECT * FROM `" . $this->p->getConfig('table') .  "` WHERE `system_id`= '{$id}'");

        $this->p->e->log($this,array('content'=>$result, 'description'=>'Payment details'));

        return $this->registerCurrent($result, $id);
    }

    protected function registerCurrent($result, $id) {
        if (empty($result)) {
            $this->p->e->log($this,array('content'=>array('id'=>$id), 'description'=>'Get payment details, payment not found'));
            return false;
        } else {
            $result = $result[0];

            $this->p->e->log($this,array('content'=>array('id'=>$id, 'result'=>$result), 'description'=>'Get payment details, payment found'));

            $this->payment_id = $result['id'];
            $this->payment_transaction_id = $result['transaction_id'];
            $this->payment_system_id = $result['system_id'];

            return $result;
        }
    }

    protected function getPayment($id, $byId = false) {

        if ($byId==true) {
            $use = 'id';
        } else {
            $use = 'transaction_id';
        }

        $this->p->e->log($this,array('content'=>array('id'=>$id), 'description'=>'Get payment details.'));

        if (empty($id)) return false;

        $result = $this->p->executeSQL("SELECT * FROM `" . $this->p->getConfig('table') .  "` WHERE `{$use}`= '{$id}'");

        $this->p->e->log($this,array('content'=>$result, 'description'=>'Payment details'));

        return $this->registerCurrent($result, $id);
    }

    protected function updateSemi($id, $status = 0, $byId = false) {
        if ($byId==true) {
            $use = 'id';
        } else {
            $use = 'transaction_id';
        }

        $this->p->e->log($this,array('content'=>array('id'=>$id, 'status'=>$status), 'description'=>'Updating payment SemiStatus.'));

        if (empty($id) || $id=='0') {
            $this->p->e->log($this,array('content'=>array('id'=>$id), 'description'=>'Transaction ID not provided'));
            return false;
        }

        $payment = $this->getPayment($id, $byId);

        $result = $this->p->executeSQL("UPDATE " . $this->p->getConfig('table') .  " SET `semistatus` = " . intval($status) . ", response_time=NOW() WHERE `{$use}`= '{$id}'");

        $this->p->e->log($this,array('content'=>array('id'=>$id, 'status'=>$status, 'result'=>$result), 'description'=>'Updating payment SemiStatus, complete.'));

        return $result;
    }

    protected function updatePayment($id, $status = 0, $byId = false, $force = false)
    {

        if ($byId==true) {
            $use = 'id';
        } else {
            $use = 'transaction_id';
        }

        $this->p->e->log($this,array('content'=>array('id'=>$id, 'status'=>$status), 'description'=>'Updating payment status.'));

        if (empty($id) || $id=='0') {
            $this->p->e->log($this,array('content'=>array('id'=>$id), 'description'=>'Transaction ID not provided'));
            return false;
        }

        $payment = $this->getPayment($id, $byId);

        if (!$force && $payment['status']>0) {
            $this->p->e->error($this,array('content'=>array('id'=>$id, 'payment'=>$payment), 'description'=>'status_already_set'));
            return false;
        }

        $result = $this->p->executeSQL("UPDATE " . $this->p->getConfig('table') .  " SET `status` = " . intval($status) . ", response_time=NOW() WHERE `{$use}`= '{$id}'");

        $this->p->e->log($this,array('content'=>array('id'=>$id, 'status'=>$status, 'result'=>$result), 'description'=>'Updating payment status, complete.'));

        return $result;
    }

    public function saveErrors($id, $errors) {

        if ($id!=null && $id!=0) {
            $this->p->executeSQL("UPDATE " . $this->p->getConfig('table') .  " SET `errors`=" . $this->p->db->escape(json_encode($errors)) . " WHERE `id`= '{$id}'");
            return true;
        } else {
            return false;
        }
    }

    protected function updatePaymentDetails($id, $details, $byId = false) {

        if ($byId==true) {
            $use = 'id';
        } else {
            $use = 'transaction_id';
        }

        $this->p->e->log($this,array('content'=>array('id'=>$id, 'status'=>$details), 'description'=>'Updating payment details'));

        if (empty($id) || $id=='0') {
            $this->p->e->log($this,array('content'=>array('id'=>$id), 'description'=>'Transaction ID not provided'));
            return false;
        }

        $payment = $this->getPayment($id, $byId);

        if (is_array($details)) {
            $details = json_encode($details);
        }
        $result = $this->p->executeSQL("UPDATE " . $this->p->getConfig('table') .  " SET `response` = " . $this->p->db->escape($details) . " WHERE `{$use}`= '{$id}'");

        $this->p->e->log($this,array('content'=>array('id'=>$id, 'status'=>$details, 'result'=>$result), 'description'=>'Updating payment details, complete.'));

        return $result;

    }

    protected function generateKey() {

        $this->p->e->log($this,array('content'=>array(), 'description'=>'Generating transaction key'));

        if ($this->settings['transactionKey']=='an') {
            list($sec, $usec) = explode(".", microtime(true));
            $time = $sec . $usec;
            $start = $this->str_baseconvert($time);
            $symbols = '0,1,2,3,4,5,6,7,8,9,q,a,z,w,s,x,e,d,c,r,f,v,t,g,b,y,h,n,u,j,m,i,k,o,l,p';
            $count = 10;
        }elseif ($this->settings['transactionKey']=='i') {
            $re = $this->p->executeSQL('SELECT MAX(`transaction_id`) as tk FROM `' . $this->p->getConfig('table') . '` WHERE `bank`=\'' . get_class($this) . '\'');

            if (empty($re)) {
                $r = 0;
            } else {
                $r = intval($re[0]['tk']);
            }

            $r++;

            $this->p->e->log($this,array('content'=>array('transactionKey'=> $r), 'description'=>'Transaction key generated.'));

            return $r;
        }else {

            list($sec, $usec) = explode(".", microtime(true));
            $start = $sec . $usec;
            $symbols = '0,1,2,3,4,5,6,7,8,9';
            $count = 4;
        }

        if (isset($this->settings['transactionKeyLen'])) {
            $count = $this->settings['transactionKeyLen'];
            $count = $count - strlen($start);
        }

        $choose = explode(",",$symbols);
        $result = $start;
        for ($a=0; $a<$count; $a++) {
            $result = $result . $choose[rand(0, count($choose)-1)];
        }

        $this->p->e->log($this,array('content'=>array('transactionKey'=>$result), 'description'=>'Transaction key generated.'));

        return $result;
    }

    public function getLastByUser($id) {
        $this->p->e->log($this,array('content'=>array('data'=>$id), 'description'=>'Finding payment'));

        $result = $this->p->executeSQL("SELECT transaction_id FROM `" . $this->p->getConfig('table') .  "` WHERE status=0 AND `creator_id`= '{$id}'");

        if (!empty($result[0])) {
            return $this->getPayment($result[0]['transaction_id']);
        } else {
            return false;
        }
    }

    public function createNew($data) {

        $this->p->e->log($this,array('content'=>array('data'=>$data), 'description'=>'Creating new payment'));

        $transaction_key = $this->generateKey();

        if (!isset($data['user_id'])) {
            $data['user_id'] = 0;
        }

        $old = $this->p->executeSQL("SELECT * FROM `" . $this->p->getConfig('table') .  "` WHERE `system_id`= '" . $data['id'] . "'");

        if (!empty($old)) {
            $this->p->e->error($this,array('content'=>array('data'=>$old), 'description'=>'system_id_already_in_use'));
            return false;
        }

        if (isset($data['lang'])) {
            $lang = $data['lang'];
        } elseif (isset($this->settings['lang'])) {
            $lang = $this->settings['lang'];
        } else {
            $lang = false;
        }

        $result = $this->p->db->insert($this->p->getConfig('table'), Array(
            'creator_id' => $data['user_id'],
            'amount' => (float)$data['amount'],
            'ip' => $_SERVER['REMOTE_ADDR'],
            'transaction_id' => $transaction_key,
            'lang'=>$lang,
            'bank'=>get_class($this),
            'system_id'=>$data['id'],
            'description'=>$data['description']
        ));

        $this->payment_id =  $this->p->db->lastId();
        $this->payment_transaction_id =  $transaction_key;
        $this->payment_system_id = $data['id'];

        $this->p->e->log($this,array('content'=>array('data'=>$result), 'description'=>'Creating new payment, complete'));

        return $transaction_key;
    }

    public function changeTransactionKey ($newKey) {
        $res = $this->p->executeSQL('UPDATE `' . $this->p->getConfig('table') .  '` SET `transaction_id`=' . $this->p->db->escape($newKey) . ' WHERE `id`=' . $this->p->db->escape($this->payment_id));
        $this->payment_transaction_id =  $newKey;
        return $res;
    }

    public function str_baseconvert($str, $frombase=10, $tobase=36) {
        $str = trim($str);
        if (intval($frombase) != 10) {
            $len = strlen($str);
            $q = 0;
            for ($i=0; $i<$len; $i++) {
                $r = base_convert($str[$i], $frombase, 10);
                $q = bcadd(bcmul($q, $frombase), $r);
            }
        }
        else $q = $str;

        if (intval($tobase) != 10) {
            $s = '';
            while (bccomp($q, '0', 0) > 0) {
                $r = intval(bcmod($q, $tobase));
                $s = base_convert($r, 10, $tobase) . $s;
                $q = bcdiv($q, $tobase, 0);
            }
        }
        else $s = $q;

        return $s;
    }

    protected function getLanguage($data) {
        if (isset($data['lang'])) {
            $used = $data['lang'];
        } else {
            $used = $this->settings['lang'];
        }

        if (isset($this->settings['langList'])) {
            if (isset($this->settings['langList'][$used])) {
                return $this->settings['langList'][$used];
            } else {
                return false;
            }
        } else {
            return $used;
        }
    }

    abstract public function confirm();

    public function error() {

        $this->p->e->error($this,array('content'=>array('post'=>$_POST, 'get'=>$_GET), 'description'=>'technical_diff'));
        return array('error'=>true);

    }

    public function canceled() {
        $this->p->e->error($this,array('content'=>array('post'=>$_POST, 'get'=>$_GET), 'description'=>'cancelled_transaction'));
    }

    public function cron() {

    }

}