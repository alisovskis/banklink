<?php

class BanklinkDb {

    var $CI = false;
    var $lastId = false;

    public function __construct() {
        $this->CI =& get_instance();
        if ($this->CI==NULL) {
            echo 'CI not yet initialized!';
            exit(0);
        }
    }

    public function execute($slq) {
        $v = $this->CI->db->query($slq);
        if (is_object($v) && 'CI_DB_mysqli_result'==get_class ($v)) {
            $v = $v->result_array();
        }
        return $v;
    }

    public function insert($table, $data) {
        $table = str_replace('#prefix#', Banklink::getData('global', 'tablePrefix'), $table);
        $datal = '';
        $first = true;
        foreach ($data as $ky => $val) {
            if (!$first) {$datal .=', ';}
            $datal .= "`" . $ky . "`=" . $this->CI->db->escape($val) . "";
            $first = false;
        }

        $d = $this->CI->db->query("INSERT INTO `" . $table  . "` SET " . $datal);
        $this->lastId = $this->CI->db->insert_id();
    }

    public function lastId() {
        return $this->lastId;
    }

    public function escape ($text) {
        return $this->CI->db->escape($text);
    }

    static public function get($name) {

        $CI =& get_instance();

        switch ($name) {
            case 'db': return $CI->db->database;
            case 'username': return $CI->db->username;
            case 'password': return $CI->db->password;
        }
    }

}