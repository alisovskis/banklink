<?php

class BanklinkDb {

        var $Yii = false;
        var $lastId = false;

    public function __construct() {
        $this->Yii = Yii::app();
        if ($this->Yii==NULL) {
            echo 'Yii not yet initialized!';
            exit(0);
        }
    }

    public function execute($sql) {
        $v = $this->Yii->db->createCommand($sql)->queryAll();
        $rs=array();
        if (count($v)>0)
        foreach($v as $item){
            $rs[]=$item['id'];

        }
        return $rs;
    }

    public function insert($table, $data) {
        $table = str_replace('#prefix#', Banklink::getData('global', 'tablePrefix'), $table);
        $this->Yii->db->createCommand->insert($table, $data);
        $this->lastId = $this->Yii->db->getLastInsertID();
    }

    public function lastId() {
        return $this->Yii->db->getLastInsertID();;
    }

    public function escape ($values) {
        $values = (array)$values;
        $escaped = array();
        foreach($values as $value) {
            if(!is_scalar($value)) {
                throw new CException('One of the values passed to values() is not a scalar.');
            }
            $escaped[] = $this->Yii->db->quoteValue($value);
        }
        return implode(', ', $escaped);
    }

    private static function getDsnAttribute($name, $dsn)
    {
        if (preg_match('/' . $name . '=([^;]*)/', $dsn, $match)) {
            return $match[1];
        } else {
            return null;
        }
    }

    static public function get($name) {
        $db = Yii::$app->getDb();
        switch ($name) {
            case 'db': return self::getDsnAttribute('dbname', $db->dsn);
            case 'username': return Yii::app()->db->username;
            case 'password': return Yii::app()->db->password;
        }
    }

}