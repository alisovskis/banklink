<?php

class Seb extends Bank
{

    var $id = 'seb';

    var $IB_SERVICE = '0002';
    var $IB_VERSION = '001';
    var $EVENTS_CAT = 'seb';

    private $PRIVATE_KEY;

    public function buy($opts)  {

        $this->p->e->log($this, ['content' => ['data' => $opts], 'description' => 'Creating a new payment']);

        if (!$this->createNew($opts)) {
            $this->p->e->error($this, ['content' => [], 'description' => 'Payment not created']);
            return ['error'=>true];
        }

        $data = Array(
            'IB_SND_ID' => $this->settings['id'],
            'IB_SERVICE' => $this->IB_SERVICE,
            'IB_VERSION' => $this->IB_VERSION,
            'IB_AMOUNT' => (double)round($opts['amount'], 2),
            'IB_CURR' => $this->p->getConfig('currency'),
            'IB_NAME' => $this->p->getConfig('receiver'),
            'IB_PAYMENT_ID' => $this->payment_transaction_id,
            'IB_PAYMENT_DESC' => $opts['description'],
        );

        $signData = $this->getSignatureData($data);
        $data['IB_CRC'] = $this->createSignature($signData);
        if ($data['IB_CRC']===false || is_array($data['IB_CRC'])) {
            $this->p->e->error($this, ['content' => ['result'=>$data['IB_CRC']], 'description' => 'seb_error_generating_crc']);
            return ['error'=>true];
        }
        $data['IB_FEEDBACK'] = $this->getFeedbackUrl();
        $data['IB_LANG'] = $this->settings['lang'];
        $this->setCRCForPayment($this->payment_transaction_id, $data['IB_CRC']);


        return array('to_url' => $this->settings['postUrl'], 'post' => $data, 'error'=>false);

    }

    private function verifySignature($signData, &$signature)
    {
        if ($pubcert = file_get_contents($this->settings['publicKeyPath']) and $pubkeyid = openssl_pkey_get_public($pubcert)) {
            $ok = openssl_verify($signData, base64_decode($signature), $pubkeyid);
            if ($ok == 1) {
                $this->p->e->log($this, ['content' => ['signData' => $signData, 'signature' => $signature], 'description' => 'Signature verified']);
                openssl_free_key($pubkeyid);
                return true;
            } elseif ($ok == 0) {
                $this->p->e->error($this, ['content' => ['signData' => $signData, 'signature' => $signature], 'description' => 'seb_signature_is_invalid']);
                openssl_free_key($pubkeyid);
                return false;
            } else {
                $this->p->e->error($this, ['content' => ['signData' => $signData, 'signature' => $signature], 'description' => 'seb_error_with_signature']);
                openssl_free_key($pubkeyid);
                return false;
            }

        } else {
            $this->p->e->error($this, ['content' => ['signData' => $signData, 'signature' => $signature], 'description' => 'seb_error_with_certificate']);
            return false;
        }
    }

    /**
     * Atbilde no bankas
     *
     */
    public function confirm()
    {
        $this->p->e->log($this, ['content' => ['post' => $_POST, 'get'=>$_GET], 'description' => 'Confirming payment']);

        if (empty($_POST)) {
            $data = $_GET;
        } else {
            $data = $_POST;
        }

        if (isset($data['IB_SERVICE'])) {
            $service = $data['IB_SERVICE'];
        } else {
            $this->p->e->error($this, ['content' => ['request' => $data], 'description' => 'seb_service_info_not_found']);
        }

        $return = ['error'=>false, 'id' => false, 'errorMessage' => false];

        $this->p->e->log($this, ['content' => [], 'description' => 'Checking answer version']);

        if ($service =='0003') {

            $this->p->e->log($this, ['content' => ['post' => $_POST, 'get'=>$_GET], 'description' => 'Use 0003 answer']);

            $fields = Array(
                'IB_SND_ID',
                'IB_SERVICE',
                'IB_VERSION',
                'IB_PAYMENT_ID',
                'IB_AMOUNT',
                'IB_CURR',
                'IB_REC_ID',
                'IB_REC_ACC',
                'IB_REC_NAME',
                'IB_PAYER_ACC',
                'IB_PAYER_NAME',
                'IB_PAYMENT_DESC',
                'IB_PAYMENT_DATE',
                'IB_PAYMENT_TIME'
            );
        } elseif ($service=='0004') {

            $this->p->e->log($this, ['content' => ['post' => $_POST, 'get'=>$_GET], 'description' => 'Use 0004 answer']);

            $fields = Array(
                'IB_SND_ID',
                'IB_SERVICE',
                'IB_VERSION',
                'IB_REC_ID',
                'IB_PAYMENT_ID',
                'IB_PAYMENT_DESC',
                'IB_FROM_SERVER',
                'IB_STATUS',
            );
        }

        $respData = Array();
        foreach ($fields AS $field) {
            $respData[$field] = $data[$field];
        }

        $this->p->e->log($this, ['content' => [$respData], 'description' => 'Fields for signature']);

        $paymentId = $respData['IB_PAYMENT_ID'];

        $signDataT = $this->getSignatureData($respData);
        $this->p->e->log($this, ['content' => ['signedData'=>$signDataT], 'description' => 'Signature calculated']);


        if ($this->verifySignature($signDataT, $data['IB_CRC'])) {
            $this->p->e->log($this, ['content' => [], 'description' => 'Signature is valid']);

            if ($respData['IB_FROM_SERVER']=='Y') {
                $this->fromServer = true;
            } else {
                $this->fromServer = false;
            }

            $paymentData = $this->getPayment($paymentId);

            if (!$paymentData) {
                $this->p->e->error($this, ['content' => ['request' => $_REQUEST], 'description' => 'payment_not_found']);
                return ['error' => true];
            }

            if ($respData['IB_SND_ID'] == 'SEBUB' && is_array($paymentData)) {
                $return['id'] = $paymentData['system_id'];
                $statusOk = false;
                if ($service == '0004') {
                    if ($respData['IB_STATUS'] == 'ACCOMPLISHED') {
                        if ($paymentData['status'] == 0) {
                            $this->updatePayment($paymentId, 1);
                            return ['error' => false, 'id' => $this->payment_system_id, 'status' => 1, 'wait' => false];
                        } else {
                            $this->p->e->error($this, ['content' => ['request' => $_REQUEST], 'description' => 'status_already_confirmed']);
                            $return = ['error' => true];
                        }
                    } elseif ($respData['IB_STATUS'] == 'CANCELLED') {
                        $this->updatePayment($paymentId, 7);
                        $this->p->e->error($this, ['content' => ['request' => $_REQUEST], 'description' => 'cancelled_transaction']);
                        $return = ['error' => true];
                    } else {
                        $this->updatePayment($paymentId, 99);
                        $this->p->e->error($this, ['content' => ['request' => $_REQUEST], 'description' => 'missing_status']);
                        $return = ['error' => true];
                    }
                } else {
                    if ($paymentData['status'] == 0) {
                        $this->p->e->log($this, ['content' => [], 'description' => 'Status Ok, must wait for finalizing status!']);
                        return ['error' => false, 'id' => $this->payment_system_id, 'status' => 0, 'wait' => true];
                    } else {
                        $this->p->e->error($this, ['content' => ['request' => $_REQUEST], 'description' => 'status_already_confirmed']);
                        $return = ['error' => true, 'status' => $paymentData['status'], 'id' => $this->payment_system_id, 'wait' => false];
                    }
                }
            } else {
                $this->p->e->error($this, ['content' => ['request' => $_REQUEST], 'description' => 'payment_not_accepted']);
                $return = ['error' => true];
            }
        } else {
            $this->p->e->error($this, ['content' => ['request' => $_REQUEST], 'description' => 'seb_signature_invalid']);
            $return = ['error' => true];
        }

        return $return;

    }

    /**
     * Saglabā uzģenerēto atslēgu tālākai pārbaudei
     *
     * @param integer $payment_id
     * @param text $crc
     */
    private function setCRCForPayment($payment_id, $crc)
    {
        return $this->p->executeSQL("UPDATE
        `" . $this->p->getConfig('table') . "`
    SET
        `crc` = '{$crc}'
    WHERE
         `transaction_id`= '{$payment_id}'
        "
        );
    }

    /**
     * Atgriež virkni pārbaudei
     *
     * @param array $data
     * @return string
     */
    private function getSignatureData(&$data)
    {
        $signLine = '';
        foreach ($data AS $key => $val) {
            $signLine .= sprintf('%03s', strlen(utf8_decode($val))) . $val;
        }
        return $signLine;
    }

    /**
     * Atgriež pieprasījuma ciparu parakstu
     *
     * @param string $signData
     * @return text
     */
    private function createSignature($signData)
    {
        $fns_res = '';
        if (file_exists($this->settings['privateKeyPath'])) {
            $this->p->e->log($this, ['content' => ['private key file' => $this->settings['privateKeyPath']], 'description' => 'Private key file found!']);

            $pkeyid = openssl_pkey_get_private ('file://' . $this->settings['privateKeyPath'], $this->settings['privateKeyPass']);
            if ($pkeyid !== false) {
                $signature = '';
                openssl_sign($signData, $signature, $pkeyid);
                $fns_res = base64_encode($signature);
                openssl_free_key($pkeyid);
            }
        } else {
            $this->p->e->error($this, ['content' => ['private key file' => $this->settings['privateKeyPath']], 'description' => 'private_key_not_found']);
            return ['error'=>true];
        }

        return $fns_res;
    }

    /**
     * Atgriež privātās atslēgas tekstu
     *
     * @return text
     */
    private function getPrivateKey()
    {
        if ($this->PRIVATE_KEY) return $this->PRIVATE_KEY;
        $this->PRIVATE_KEY = file_get_contents($this->settings['privateKeyPath']);
        return $this->PRIVATE_KEY;
    }

    /**
     * Atgriež saiti atbildei no bankas
     *
     * @return string
     */
    private function getFeedbackUrl()
    {
        $url = $this->settings['responseUrl'];

        $url .= $this->settings['ibFeedback'];

        if (isset($_GET['remove_css_fix_for_api_extend'])) {
            if (preg_match('/\?/s', $url)) {
                $url .= '&';
            } else {
                $url .= '?';
            }
        }

        return $url;
    }

}