<?php

class Firstdata extends Bank
{

    public $id = 'firstdata';

    var $mercahnt = false;

    public function __construct($parent,$settings) {
        parent::__construct($parent, $settings);

        include_once 'service/Merchant.php';

        $this->merchant = new Merchant($this->settings['serverUrl'], $this->settings['privateKeyPath'], $this->settings['privateKeyPass'], 1);

    }

    protected function generateKey() {
        return $this->payment_transaction_id;
    }

    public function buy($data) {
        if ($this->settings['type']=='DMS') {
            $this->p->e->log($this, ['content' => [], 'description' => 'Starting DMS buy transaction.']);
        } else {
            $this->p->e->log($this, ['content' => [], 'description' => 'Starting SMS buy transaction.']);
        }

        $this->p->e->log($this, ['content' => ['data' => $data], 'description' => 'Creating a new payment']);

        $desc = urlencode(htmlspecialchars($data['description'], ENT_QUOTES));

        if ($this->settings['ip']) {
            $ip = $this->settings['ip'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        $this->p->e->log($this, ['content' => ['client IP' => $ip], 'description' => 'IP Used']);

        if ($this->settings['type']=='DMS') {
            $this->p->e->log($this, ['content' => ['data' => $data], 'description' => 'Trying to do DMSAuth request']);

            $resp = $this->merchant->startDMSAuth(intval($data['amount'] * 100), $this->settings['currency'],$ip, $desc, $this->getLanguage($data));
        } else {
            $this->p->e->log($this, ['content' => ['data' => $data], 'description' => 'Trying to do SMSTrans request']);
            $resp = $this->merchant->startSMSTrans(intval($data['amount'] * 100), $this->settings['currency'], $ip, $desc, $this->getLanguage($data));
        }

        if (strpos($resp, 'error')!==false) {

            $this->p->e->error($this, ['content' => ['data' => $data, 'response' => $resp], 'description' => 'Error received: ' . $resp]);
            $this->p->e->error($this, ['content' => [], 'description' => 'fd_could_not_create_transaction']);
            return ['error'=>true];

        } else {

            $this->p->e->log($this, ['content' => ['response' => $resp], 'description' => 'Creating a new payment, request sent']);

            $resp = $this->convertToArray($resp);

            $this->payment_transaction_id = $resp['transaction_id'];

            if (!$this->createNew($data)) {

                $this->p->e->error($this, ['content' => ['data' => $data, 'response' => $resp], 'description' => 'fd_could_not_create_payment']);
                return ['error'=>false];
            }

            $this->p->e->log($this, ['content' => ['transaction_id' => $this->payment_transaction_id], 'description' => 'Verifying payment']);

            $respo = $this->merchant -> getTransResult(urlencode($this->payment_transaction_id), $ip);

            $respo = $this->convertToArray($respo);

            if ($respo['result']=='OK' || $respo['result']=='CREATED' || $respo['result']=='PENDING') {

                $this->p->e->log($this, ['content' => ['response' => $respo], 'description' => 'Verifying payment, Success']);

                return ['error' => false, 'to_url' => $this->settings['postUrl'], 'post' => ['trans_id' => $this->payment_transaction_id]];

            } else {
                $err=[
                    'FAILED'=>2,
                    'DECLINED'=>3,
                    'REVERSED'=>4,
                    'AUTOREVERSED'=>5,
                    'TIMEOUT'=>6
                ];

                $this->p->e->error($this, ['content' => ['response' => $respo], 'description' => 'could_not_verify_payment']);

                $this->updatePayment($this->transaction_id, $err[$respo['result']]);

                return ['error'=>true];
            }
        }
    }

    private function convertToArray($re) {
        $res = explode("\n", $re);

        $arr = [];

        foreach ($res as $r) {
            $r = explode(":", $r);
            if (count($r)>1) {
                $arr[strtolower(trim($r[0]))] = trim($r[1]);

                if (strtolower(trim($r[0]))=='error') {
                    $this->p->e->error($this, ['content' => ['received' => $re], 'description' => 'Error received: ' . trim($r[1])]);
                }
            }
        }

        return $arr;
    }

    public function reverse($id) {

        $entry = $this->bySystemId($id);

        if (!$entry) {
            $this->p->e->error($this, ['content' => ['id' => $id], 'description' => 'payment_not_found']);
            return ['error'=>true];
        }

        if ($entry['status']==1) {

            $this->p->e->log($this, ['content' => ['id' => $this->payment_transaction_id, 'amount'=>$entry['amount']], 'description' => 'Trying to reverse!']);
            $re = $this->merchant->reverse($this->payment_transaction_id, $entry['amount'] * 100);
            $this->p->e->log($this, ['content' => ['response' => $re], 'description' => 'Reversing call ended!']);

            $result = $this->convertToArray($re);

            if (isset($result['error'])) {
                return ['error'=>true];
            }

            if ($result['result']=='OK' || $result['result']=='REVERSED') {

                $this->updatePayment($this->payment_transaction_id, 100, false, true);

                return true;
            } else {
                if ($result['result_code']==914) {
                    $this->p->e->error($this, ['content' => [], 'description' => 'fd_payment_too_old']);
                } else {
                    $this->p->e->error($this, ['content' => [], 'description' => 'fd_unidentified_error']);
                }
                return ['error'=>true];
            }

        } else {
            $this->p->e->error($this, ['content' => ['entry' => $entry], 'description' => 'fd_status_is_not_confirmed']);
            return ['error'=>true];
        }

    }

    public function echoData($a) {
        if ($this->settings['printForTest']) {
            echo $a . "<br/>\n\r";
        }
    }


    public function confirm($forceData = false) {
        if ($this->settings['type']=='DMS') {
            $this->p->e->log($this, ['content' => [], 'description' => 'Starting DMS confirm transaction.']);
            $this->echoData("Transaction type: DMS");
        } else {
            $this->p->e->log($this, ['content' => [], 'description' => 'Starting SMS confirm transaction.']);
            $this->echoData('Transaction type: SMS');
        }

        if ($this->settings['ip']) {
            $ip = $this->settings['ip'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        $this->echoData('Transaction id: ' . $_POST['trans_id']);

        if ($forceData) {
            $_POST = $forceData;
            $this->p->e->log($this, ['content' => [], 'description' => 'Forcing Data!']);
        }
        $this->p->e->log($this, ['content' => ['post'=>$_POST], 'description' => 'Confirming payment']);

        $payment = $this->getPayment($_POST['trans_id']);

        if (!empty($payment)) {
            if ($payment['status']==0) {

                $this->p->e->log($this, ['content' => ['post' => $_POST], 'description' => 'Payment found, starting auth.']);
                $desc = urlencode(htmlspecialchars($payment['description'], ENT_QUOTES));

                $transId = urlencode($_POST['trans_id']);

                $this->p->e->debug($this, ['content' => ['transIdEncoded' => $transId], 'description' => 'Trans id Encoded']);

                if ($this->settings['type']=='DMS') {
                    $result = $this->merchant->makeDMSTrans($transId, intval($payment['amount'] * 100), $this->settings['currency'], $ip, $desc, $this->getLanguage($payment));

                    $this->echoData('Transaction result: ' . $result);

                    $result = $this->convertToArray($result);

                    if (isset($respo['error'])) {
                        $this->p->e->error($this, ['content' => ['response' => $respo], 'description' => 'Error received:' . $result['error']]);
                        return ['error' => true];
                    }

                    if (isset($result['result']) && $result['result'] == 'OK') {

                        $this->p->e->log($this, ['content' => ['response' => $result], 'description' => 'Auth was success.']);
                        $this->updatePayment($this->payment_transaction_id, 1);
                        return ['error' => false, 'id' => $this->payment_system_id];

                    } else {
                        $this->p->e->error($this, ['content' => ['response' => $result], 'description' => 'fd_auth_failed']);
                        $this->updatePayment($this->payment_transaction_id, 7);

                        return ['error' => true];

                    }
                } else {
                    $respo = $this->merchant -> getTransResult(urlencode($this->payment_transaction_id), $ip);

                    $this->echoData('Transaction result: ' . $respo);

                    $respo = $this->convertToArray($respo);

                    if (isset($respo['error'])) {
                        $this->p->e->error($this, ['content' => ['response' => $respo], 'description' => 'Error received:' . $result['error']]);
                        return ['error' => true];
                    }

                    if (isset($respo['result']) && $respo['result']=='OK') {

                        $this->p->e->log($this, ['content' => ['response' => $respo], 'description' => 'Verifying payment, Success']);

                        $this->updatePayment($this->payment_transaction_id, 1);

                        return ['error' => false, 'id' => $this->payment_system_id];

                    } else {
                        $err=[
                            'FAILED'=>2,
                            'DECLINED'=>3,
                            'REVERSED'=>4,
                            'AUTOREVERSED'=>5,
                            'TIMEOUT'=>6,
                            'CREATED'=>20,
                            'PENDING'=>21
                        ];

                        $this->p->e->error($this, ['content' => ['response' => $respo], 'description' => 'could_not_verify_payment']);

                        $this->updatePayment($this->payment_transaction_id, $err[$respo['result']]);

                        return ['error'=>true];
                    }
                }
            } else {
                $this->p->e->error($this, ['content' => ['payment' => $payment, 'post'=>$_POST], 'description' => 'payment_not_found']);
                return ['error'=>true];
            }
        } else {
            $this->p->e->error($this, ['content' => ['payment' => $payment, 'post'=>$_POST], 'description' => 'payment_not_found']);
            return ['error'=>true];
        }
    }

    public function error() {

        $this->p->e->log($this, ['content' => ['post' => $_POST, 'get'=>$_GET], 'description' => 'Got payment error']);

        if (isset($_POST['trans_id'])) {
            $payment = $this->getPayment($_POST['trans_id']);

            if (!empty($payment) && !$this->updatePayment($this->payment_transaction_id, 99)) {
                $this->p->e->error($this, ['content' => [], 'description' => 'could_not_update']);
                return ['error' => true];
            }
        } else {
            $this->p->e->error($this, ['content' => [], 'description' => 'missing_transaction_id']);
            return ['error' => true];
        }

        return ['error'=>false, 'id'=>$this->payment_system_id];

    }

    public function cron() {
        $resp = $this->merchant -> closeDay();

        $resp = $this->convertToArray($resp);

        if ($resp['result']=='OK') {
            $this->p->e->log($this, ['content' => ['response' => $resp], 'description' => 'Day compleated!']);
            return ['error'=>false];
        } else {
            $this->p->e->error($this, ['content' => ['response' => $resp], 'description' => 'failed_dail_closure']);
            return ['error'=>true, 'result'=>$resp];
        }
    }
}