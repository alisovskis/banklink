<?php

class Nordea extends Bank {

    var $id = 'nordea';

    /* ---- BASE Functions --- */

    // Buy function

    public function buy($data) {

        $this->p->e->log($this, ['content' => ['data' => $data], 'description' => 'Creating a new payment']);

        if (!($this->createNew($data))) {
            return ['error'=>true];
        }

        $this->p->e->log($this, ['content' => [], 'description' => 'Creating a reference']);
        $ref = $this->createReference();

        $send = [
        'VERSION'=>$this->settings['paymentVersion'],
          'STAMP'=>$this->payment_transaction_id,
          'RCV_ID'=>$this->settings['id'],
            'LANGUAGE'=>$this->getLanguage($data),
            'AMOUNT'=>$data['amount'],
            'REF'=>$ref,
            'DATE'=>date('d.m.Y', time()),
            'MSG'=>substr($data['description'],0, 220),
            'RETURN'=>$this->settings['responseUrl'],
            'CANCEL'=>$this->settings['cancelUrl'],
            'REJECT'=>$this->settings['failUrl'],
            'CONFIRM'=>'YES',
            'KEYVERS'=>$this->settings['macKeyVersion'],
            'CUR'=>$this->p->getConfig('currency')
        ];

        $send['MAC'] = $this->createCode($send, 'send');

        $this->p->e->log($this, ['content' => [$send], 'description' => 'Created a mac code, sending answer.']);

        return ['error'=>false, 'to_url'=>$this->settings['postUrl'], 'post'=>$send];
    }

    // Confirm request function

    public function confirm() {

        $this->p->e->log($this, ['content' => ['post' => $_POST, 'get'=>$_GET], 'description' => 'Confirming payment']);

        $this->p->e->log($this, ['content' => [], 'description' => 'Validating']);

        if (!empty($_POST)) {
            $method = $_POST;
        } else {
            $method = $_GET;
        }

        if ($this->validate($method)) {

            $this->p->e->log($this, ['content' => [], 'description' => 'Data validated successfully']);

            $paymentData = $this->getPayment($method['RETURN_STAMP']);

            if (!$paymentData) {
                $this->p->e->error($this, ['content' => ['request' => $_REQUEST], 'description' => 'payment_not_found']);
                return ['error'=>true];
            }

            if (!$this->updatePayment($this->payment_transaction_id, 1)) {
                $this->p->e->error($this, ['content' => [], 'description' => 'could_not_update']);
                return ['error'=>true];
            }

            return ['error'=>false, 'id'=>$this->payment_system_id];

        } else {

            $this->p->e->error($this, ['content' => [], 'description' => 'could_not_validate']);
            return ['error'=>true];

        }
    }

    public function requireStatus($system_id) {

        $paymentData = $this->bySystemId($system_id);

        if (!$paymentData) {
            $this->p->e->error($this, ['content' => ['request' => $_REQUEST], 'description' => 'payment_not_found']);
            return ['error'=>true];
        }

        $e = microtime(true);
        $mt = explode(".", $e);

        $ref = $this->createReference();

        if ($this->p->getConfig('production')) {
            $send = [
                'SOLOPMT_VERSION' => '0001',
                'SOLOPMT_TIMESTAMP' => date('YmdHis', time()) . $mt[1],
                'SOLOPMT_RCV_ID' => $this->settings['id'],
                'SOLOPMT_LANGUAGE' => $this->getLanguage([]),
                'SOLOPMT_RESPTYPE' => 'xml',
                'SOLOPMT_STAMP' => $this->payment_transaction_id,
                'SOLOPMT_REF' => $ref,
                'SOLOPMT_KEYVERS' => $this->settings['macKeyVersion'],
                'SOLOPMT_ALG' => "01"
            ];
        } else {
            $send = [
                'SOLOPMT_VERSION' => '0001',
                'SOLOPMT_TIMESTAMP' => '200901271459010001',
                'SOLOPMT_RCV_ID' => '12345678',
                'SOLOPMT_LANGUAGE' => 4,
                'SOLOPMT_RESPTYPE' => 'xml',
                'SOLOPMT_STAMP' => '1233059715344',
                'SOLOPMT_REF' => '1232',
                'SOLOPMT_KEYVERS' => '0001',
                'SOLOPMT_ALG' => "01"
            ];
        }

        $this->p->e->log($this, ['content' => ['post' => $send], 'description' => 'Try to get status']);

        $send['SOLOPMT_MAC'] = $this->createCode($send, 'request');


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, trim($this->settings['postUrlQuery']));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);

        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt( $ch, CURLOPT_COOKIESESSION, true );
        curl_setopt( $ch, CURLOPT_COOKIEJAR, $this->p->dir . "/tmp/" . $mt . ".txt");
        curl_setopt( $ch, CURLOPT_COOKIEFILE, $this->p->dir . "/tmp/" . $mt . ".txt");

        curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_FILETIME, true);
        curl_setopt($ch,CURLOPT_STDERR, $verbose = fopen('php://temp', 'rw+'));
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.12) Gecko/20101026 Firefox/3.6.12');

       // var_dump($send);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $send);
        //curl_setopt($ch, CURLOPT_HTTPHEADER,     array('Content-Type: text/html'));

        $output = curl_exec($ch);

        $this->p->e->log($this, ['content' => ['error'=>curl_error ($ch),'sentData'=>$send, 'url'=>$this->settings['postUrlQuery'], 'result'=>$output], 'description' => 'Response data']);
     //   var_dump($output);

        //  var_dump(curl_getinfo($ch, CURLINFO_HTTP_CODE));
        curl_close($ch);

       return ['error'=>false, 'id'=>$system_id, 'fromServer'=>-1, 'status'=>0, 'wait'=>false];
    }

    // Error request function

    public function error() {

        $this->p->e->log($this, ['content' => ['post' => $_POST, 'get'=>$_GET], 'description' => 'Got payment error']);

        $this->p->e->log($this, ['content' => [], 'description' => 'Validating']);

        if (!empty($_POST)) {
            $method = $_POST;
        } else {
            $method = $_GET;
        }

        if ($this->validate($method)) {

            $this->p->e->log($this, ['content' => [], 'description' => 'Data validated successfully']);

            $this->getPayment($method['RETURN_STAMP']);

            if (!$this->updatePayment($this->payment_transaction_id, 99)) {
                $this->p->e->error($this, ['content' => [], 'description' => 'could_not_update']);
                return ['error'=>true];
            }

            return ['error'=>false,  'id'=>$this->payment_system_id];

        } else {

            $this->p->e->error($this, ['content' => [], 'description' => 'could_not_validate']);
            return ['error'=>true];

        }

    }

    // Canceled request function

    public function canceled() {

        $this->p->e->log($this, ['content' => ['post' => $_POST, 'get'=>$_GET], 'description' => 'Got payment canceled']);

        $this->p->e->log($this, ['content' => [], 'description' => 'Validating']);

        if (!empty($_POST)) {
            $method = $_POST;
        } else {
            $method = $_GET;
        }

        if ($this->validate($method)) {

            $this->p->e->log($this, ['content' => [], 'description' => 'Data validated successfully']);

            $this->getPayment($method['RETURN_STAMP']);

            if (!$this->updatePayment($this->payment_transaction_id, 7)) {
                $this->p->e->error($this, ['content' => [], 'description' => 'could_not_update']);
                return ['error'=>true];
            }

            return ['error'=>false, 'id'=>$this->payment_system_id];

        } else {

            $this->p->e->error($this, ['content' => [], 'description' => 'could_not_validate']);
            return ['error'=>true];

        }

    }

    /* ---- Individual Functions --- */

    // Create required reference code from timestamp as per specification

    private function createReference() {

        $text = $this->settings['refCalc'];
        $b = 0;

        $total = 0;

        $ids = strrev($this->payment_transaction_id);

        for ($a = 0; $a<strlen($ids); $a++) {
            $total = $total + ($ids[$a] * $text[$b]);

            if ($b==2) {
                $b=0;
            } else {
                $b++;
            }
        }

        $next = (ceil($total/10))*10;

        $key = $next - $total;

        return $this->payment_transaction_id . $key;

    }

    private function encode($data) {
        if ($this->settings['encodeUse']=='SHA1') {
            return sha1($data);
        } else {
            return md5($data);
        }
    }

    private function createCode($data, $position) {

        $streams = [
            'send'=>[
                'VERSION',
                'STAMP',
                'RCV_ID',
                'AMOUNT',
                'REF',
                'DATE',
                'CUR'
            ],
            'receive'=>[
                'RETURN_VERSION',
                'RETURN_STAMP',
                'RETURN_REF',
                'RETURN_PAID'
            ],
            'request'=>[
                'SOLOPMT_VERSION',
                'SOLOPMT_TIMESTAMP',
                'SOLOPMT_RCV_ID',
                'SOLOPMT_LANGUAGE',
                'SOLOPMT_RESPTYPE',
                'SOLOPMT_STAMP',
                'SOLOPMT_REF',
                'SOLOPMT_KEYVERS',
                'SOLOPMT_ALG'
            ]
        ];

        $string = '';

        foreach ($streams[$position] as $a) {
            if (!isset($data[$a])) {
                if ($position!='$position') {
                    $string .= "&";
                }
            } else {
                $string .= $data[$a] . "&";
            }
        }

        $string .= $this->settings['mac'] . '&';
        $result = $this->encode($string);

        return strtoupper($result);

    }

    private function validate($method) {

        $result = $this->createCode($method, 'receive');

        if ($result==$method['RETURN_MAC']) {
            return true;
        } else {
            return false;
        }
    }



}