<?php

class Norvik extends Bank {

    /* ---- BASE Functions --- */

    var $id = 'norvik';

    // Buy function

    public function buy($data) {

        $this->p->e->log($this, ['content' => ['data' => $data], 'description' => 'Creating a new payment']);

        if (!($this->createNew($data))) {
            return ['error'=>true];
        }

        $this->p->e->log($this, ['content' => [], 'description' => 'Creating payment data']);

        $sendData = [
            'snd'=>$this->settings['receiver'],
            'query'=>'NEW',
            'type'=>$this->settings['type'],
            'data'=>[
                'CLIENT_ID'=>$this->settings['id'],
                'AMOUNT'=>floatval($data['amount']),
                'CCY_NAME'=>$this->settings['currency'],
                'CCY_CODE'=>$this->settings['currencyCode'],
                'BNF_NAME'=>$this->settings['legalName'],
                'BNF_ACCOUNT'=>$this->settings['legalAccount'],
                'BNF_ID'=>$this->settings['legalId'],
                'RESIDENCE_ID'=>$this->settings['legalCountry'],
                'RESIDENCE_NAME'=>$this->settings['legalCountryName'],
                'DETAILS'=>htmlspecialchars($data['description'])
            ],
            'date'=>date('dmY', time()),
            'time'=>date('His', time()),
            'reply_url'=>$this->settings['responseUrl'],
            'reply'=>[
                'session_id'=>$this->payment_transaction_id
            ]
        ];

        $send['DATA'] = $this->createXML($sendData);
        $send['LANG'] = $this->getLanguage($data);

        $this->p->e->log($this, ['content' => ['send'=>$send], 'description' => 'Finished creating payment data']);

        $send['MAC'] = $this->calculateMac($send['DATA']);

        if (!$send['MAC']) {
            $this->p->e->error($this, ['content' => [], 'description' => 'could_not_generate_mac']);
            return ['error'=>true];
        }

        $this->p->e->log($this, ['content' => [$send], 'description' => 'Created a mac code.']);

        return ['error'=>false, 'to_url'=>$this->settings['postUrl'], 'post'=>$send];
    }

    private function calculateMac($data) {
        $this->p->e->log($this, ['content' => [], 'description' => 'Creating mac']);
       // $data = sha1($data);

        $this->p->e->log($this, ['content' => [], 'description' => 'Checking for private key file']);

        if (($this->settings['privateKeyPath'])!=='') {

           if (file_exists($this->settings['privateKeyPath'])) {

               $key = openssl_pkey_get_private("file://" . $this->settings['privateKeyPath'], $this->settings['privateKeyPass']);
               $this->p->e->log($this, ['content' => [], 'description' => 'Private key acquired!']);
               $signed = openssl_sign($data, $signature, $key, 'sha1WithRSAEncryption');
               $result = base64_encode($signature);

           } else {
               $this->p->e->error($this, ['content' => [$this->settings['privateKeyPath']], 'description' => 'private_key_not_found']);
               return false;
           }

        } else {
            $this->p->e->error($this, ['content' => [], 'description' => 'private_key_not_set']);
            return false;
        }

        return $result;

    }

    // Confirm request function

    public function confirm() {

        $this->p->e->log($this, ['content' => ['post' => $_POST, 'get'=>$_GET], 'description' => 'Confirming payment']);

        $this->p->e->log($this, ['content' => [], 'description' => 'Validating']);

        if (!empty($_POST)) {
            $method = $_POST;
        } else {
            $method = $_GET;
        }

        if ($this->validate($method)) {

            $this->p->e->log($this, ['content' => [], 'description' => 'Data validated successfully']);

            $data = $this->xmlToArray($method['DATA']);

            $this->p->e->log($this, ['content' => [$data], 'description' => 'Extracted data']);

            $this->p->e->log($this, ['content' => [], 'description' => 'Verifying MAC']);

            //$mac = $this->calculateMac($method['DATA']);

            $mac = $this->verifyMac($method['DATA'], $method['MAC']);

            if (!$mac) {
                $this->p->e->error($this, ['content' => [], 'description' => 'nv_mac_verification_failed']);
                return ['error'=>truem];
            }

            $paymentData = $this->getPayment($data['reply']['session_id']);

            if (!$paymentData) {
                $this->p->e->error($this, ['content' => ['request' => $data['reply']], 'description' => 'payment_not_found']);
                return ['error'=>true];
            }


            if ($data['status']==12) {
                $status = 1;
                $subStatus = 1;
            }

            if ($data['status']==20) {
                $status = 3;
                $subStatus = 3;
            }

            if ($data['status']==10) {
                //Signed (in progress)
                $status = 0;
                $subStatus = 10;
            }

            if ($data['status']==11) {
                //Processing (in progress)
                $status = 0;

                $subStatus = 11;
            }

            $this->updateSemi($this->payment_transaction_id, $subStatus);

            if (!$this->updatePayment($this->payment_transaction_id, $status)) {
                $this->p->e->error($this, ['content' => [], 'description' => 'could_not_update']);
                return ['error'=>true];
            }

            if ($status==0) {
                return ['error' => false, 'id' => $this->payment_system_id, 'status'=>$status, 'wait'=>true];
            } else {
                return ['error' => false, 'id' => $this->payment_system_id, 'status'=>$status, 'wait'=>false];
            }

        } else {

            $this->p->e->error($this, ['content' => [], 'description' => 'could_not_validate']);
            return ['error'=>true];

        }
    }

    function createXML($data) {

        $out = '<?xml version="1.0" encoding="utf-8"?><bl>';

        foreach ($data as $k=>$v) {

            $out .= '<' . $k . '>';
            if (is_array($v)) {
                foreach ($v as $dk => $dv) {
                    $out .=  '<' . $dk . '>';
                    $out .=  $dv;
                    $out .=  '</' . $dk . '>';
                }
            } else {
                $out .= $v;
            }

            $out .= '</' . $k . '>';

        }

        $out .= '</bl>';

        return $out;

    }

    // Error request function

    public function error() {

        $this->p->e->log($this, ['content' => ['post' => $_POST, 'get'=>$_GET], 'description' => 'Got payment error']);

        $this->p->e->log($this, ['content' => [], 'description' => 'Validating']);

        if (!empty($_POST)) {
            $method = $_POST;
        } else {
            $method = $_GET;
        }

        if ($this->validate($method)) {

            $this->p->e->log($this, ['content' => [], 'description' => 'Data validated successfully']);

            $this->getPayment($method['SOLOPMT_RETURN_STAMP']);

            if (!$this->updatePayment($this->payment_transaction_id, 99)) {
                $this->p->e->error($this, ['content' => [], 'description' => 'could_not_update']);
                return ['error'=>true];
            }

            return ['error'=>false, 'id'=>$this->payment_system_id];

        } else {

            $this->p->e->error($this, ['content' => [], 'description' => 'could_not_validate']);
            return ['error'=>true];

        }

    }

    private function validate($method) {
        return true;
    }

    private function verifyMac($signData, &$signature)
    {
        if ($pubcert = file_get_contents($this->settings['publicKeyPath']) and $pubkeyid = openssl_pkey_get_public($pubcert)) {
            $ok = openssl_verify($signData, base64_decode($signature), $pubkeyid, 'sha1WithRSAEncryption');
            if ($ok == 1) {
                $this->p->e->log($this, ['content' => ['signData' => $signData, 'signature' => $signature], 'description' => 'MAC verified']);
                openssl_free_key($pubkeyid);
                return true;
            } elseif ($ok == 0) {
                $this->p->e->error($this, ['content' => ['signData' => $signData, 'signature' => $signature], 'description' => 'nk_signature_is_invalid']);
                openssl_free_key($pubkeyid);
                return false;
            } else {
                $this->p->e->error($this, ['content' => ['signData' => $signData, 'signature' => $signature], 'description' => 'nk_error_with_signature']);
                openssl_free_key($pubkeyid);
                return false;
            }

        } else {
            $this->p->e->error($this, ['content' => ['signData' => $signData, 'signature' => $signature], 'description' => 'nk_error_with_certificate']);
            return false;
        }
    }

}