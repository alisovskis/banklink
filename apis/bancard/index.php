<?php

class Bancard extends Bank {

    var $id = 'bancard';

    public function buy($data) {
        $this->p->e->log($this, array('content' => array('data' => $data), 'description' => 'Creating a new payment'));

        if (!($this->createNew($data))) {
            return array('error'=>true);
        }

        $send = array(
            'public_key'=>$this->settings['publicKey'],
            'operation' => array(
                'shop_process_id'=>$this->payment_id,
                'currency'=>$this->p->getConfig('currency'),
                'amount'=>number_format($data['amount'], 2, ".", ""),
                'additional_data'=>'',
                'description'=>$data['description'],
                'return_url'=>$this->settings['responseUrl'],
                'cancel_url'=>$this->settings['cancelUrl']
            )
        );

        $this->p->e->log($this,  array('content' =>  array($send), 'description' => 'Created a token, sending answer.'));
        $send['operation']['token'] = md5($this->settings['privateKey'] . $this->payment_id . number_format($data['amount'], 2, ".", "") . $this->p->getConfig('currency'));

        $url = $this->settings['postUrl'] . "/vpos/api/0.3/single_buy";

        $this->p->e->log($this, array('content' => array($url), 'description' => 'Start curl generation'));
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, trim($url));
        if ($this->settings['postPort']!=false) {
            curl_setopt($ch, CURLOPT_PORT, $this->settings['postPort']);
        }
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($send));
        curl_setopt($ch, CURLOPT_HTTPHEADER,array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLOPT_FILETIME, true);
        curl_setopt($ch, CURLOPT_STDERR, $verbose = fopen('php://temp', 'rw+'));

        $this->p->e->log($this, array('content' => array(), 'description' => 'Send curl'));
        $result = curl_exec($ch);

        echo $_SERVER['SERVER_ADDR'];
        var_dump(curl_getinfo($ch));
        var_dump(curl_error($ch));
        echo 'XXXXXXXXXXXXXXXXXXXXXX';


        $result = json_decode($result, true);

        $this->p->e->log($this, array('content' => $result, 'description' => 'Curl answer'));

        $_SESSION['buyStoreData'][] = $this->payment_id;

        curl_close($ch);
        if($result['status']=='success') {
            $this->changeTransactionKey($result['process_id']);
            return array('error' => false, 'to_url' => $this->settings['postUrl'] . '/payment/single_buy?process_id=' . urlencode($result['process_id']), 'post' => array());
        } else {
            $this->p->e->error($this, array('content' => array('data' => $result), 'description' => 'error_creating_token'));
            return array('error'=>true);
        }
    }

    public function requireStatus($system_id) {

        $paymentData = $this->bySystemId($system_id);

        if (!$paymentData) {
            $this->p->e->error($this, ['content' => ['request' => $_REQUEST], 'description' => 'payment_not_found']);
            return ['error'=>true];
        }
        $send = array(
            'public_key'=>$this->settings['publicKey'],
            'operation' => array(
                'shop_process_id'=>$this->payment_id,
            )
        );

        $this->p->e->log($this,  array('content' =>  array($send), 'description' => 'Created a token, sending answer.'));
        $send['operation']['token'] = md5($this->settings['privateKey'] . $this->payment_id . 'get_confirmation');

        $url = $this->settings['postUrl'] . "/vpos/api/0.3/single_buy/confirmations";

        $this->p->e->log($this, array('content' => array($url), 'description' => 'Start curl generation'));
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, trim($url));
        if ($this->settings['postPort']!=false) {
            curl_setopt($ch, CURLOPT_PORT, $this->settings['postPort']);
        }
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($send));
        curl_setopt($ch, CURLOPT_HTTPHEADER,array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLOPT_FILETIME, true);
        curl_setopt($ch, CURLOPT_STDERR, $verbose = fopen('php://temp', 'rw+'));

        $this->p->e->log($this, array('content' => array(), 'description' => 'Send curl'));
        $result = curl_exec($ch);

        $result = json_decode($result, true);

        $this->p->e->log($this, array('content' => $result, 'description' => 'Curl answer'));

        if ($result['status']=='success') {

            $return = $this->setAnswer($result['confirmation'], $result);
            $updateResult = $return['updateResult'];
            $error = $return['error'];

            $this->updatePaymentDetails($this->payment_transaction_id, $result);

            return ['error'=>false, 'id'=>$system_id, 'fromServer'=>-1, 'status'=>0, 'wait'=>false];

        } else {
            if ($result['messages'][0]['key']=='AlreadyRollbackedError') {
                $this->updatePayment($this->payment_transaction_id, 100, false, true);
                $this->p->e->error($this, array('content' => $result, 'description' => 'bc_error_reversed'));
                return array('error'=>true);
            }
            $this->p->e->error($this, array('content' => $result, 'description' => 'bc_error_confirming'));
            return array('error'=>true);
        }

    }

    public function confirm() {

        if (!isset($_GET['output'])) {
            $this->p->e->error($this, array('content' => array(), 'description' => 'bc_ask_for_details'));
            return array('error'=>true);
        }

        $response = $this->validateRequest();

        $this->p->e->log($this, array('content' => $response, 'description' => 'Check for error'));
        if (isset($response['error'])) {
            return $response;
        }
        $error = false;

        $this->p->e->log($this, array('content' => $response, 'description' => 'Pick answer'));

        $result = $this->setAnswer($response['answer']['operation'], $response);
        $updateResult = $result['updateResult'];
        $error = $result['error'];

        if (!$updateResult) {
            $error = 'could_not_update';
        }

        if ($error) {
            $this->p->e->error($this, array('content' => array('data' => $response), 'description' => $error));
            return array('error'=>true);
        }

    }

    public function setAnswer($response, $full) {
        $error = false;
        $updateResult = true;

        if ($response['response']=="N") {
            $updateResult = $this->updatePayment($this->payment_transaction_id, 16);
            $error = 'bc_response_statuss_n';
        } else {

            if ($response['response_code'] == "00") {
                if (!$this->updatePayment($this->payment_transaction_id, 1)) {
                    return array('error' => false, 'status' => 1, 'id' => $this->payment_id);
                } else {
                    $this->p->e->error($this, array('data' => $full, 'description' => 'could_not_update'));
                    return array('error' => true);
                }
            }

            if ($response['response_code'] == "05") {
                $updateResult = $this->updatePayment($this->payment_transaction_id, 12);
                $error = 'bc_card_disabled';
            }

            if ($response['response_code'] == "12") {
                $updateResult = $this->updatePayment($this->payment_transaction_id, 13);
                $error = 'bc_invalid_transaction';
            }

            if ($response['response_code'] == "15") {
                $updateResult = $this->updatePayment($this->payment_transaction_id, 14);
                $error = 'bc_card_invalid';
            }

            if ($response['response_code'] == "51") {
                $updateResult = $this->updatePayment($this->payment_transaction_id, 15);
                $error = 'bc_insufficient_funds';
            }

            if (!in_array($response['response_code'], array("00", "05", '12', '15', '51'))) {
                $updateResult = $this->updatePayment($this->payment_transaction_id, 15);
                $error = 'BC, Unknown Error:' . $response['response_code'];
            }
        }

        return ['updateResult'=>$updateResult, 'error'=>$error];
    }

    public function canceled($add = false) {

        $error = false;

        if ($add && $add['user_id']) {
            $got = $this->getLastByUser($add['user_id']);

            if (!$got) {
                $this->p->e->error($this, array('content' => array('data' => $got), 'description' => 'Could not find unfinished transactions'));
                return array('error'=>true);
            }
        }

        $error = $this->reverse($got['system_id']);

        return true;
    }

    public function validateRequest(){
        $answerBody = file_get_contents('php://input');
        $this->p->e->log($this, array('content' => array($_GET, $_POST, $answerBody), 'description' => 'Starting confirm transaction'));

        $answer = json_decode($answerBody, true);

        $this->p->e->log($this, array('content' => array($answer['operation']['shop_process_id']), 'description' => 'Try to find transaction'));

        $payment = $this->getPayment($answer['operation']['shop_process_id'], true);
        if (!$payment) {
            $this->p->e->error($this, array('content' => array('payment' => $payment), 'description' => 'payment_not_found'));
            return array('error'=>true);
        }

        $tokenSource = $this->settings['privateKey'] . $this->payment_id . "confirm" . number_format($payment['amount'], 2, ".", "") . $this->p->getConfig('currency');
        $this->p->e->log($this, array('content' => array('token'=>$tokenSource, 'data'=>$payment), 'description' => 'Calculating token'));
        $token = md5($tokenSource);

        if ($token!=$answer['operation']['token']) {
            $this->p->e->error($this, array('content' => $token, 'description' => 'token_does_not_match'));
            return array('error'=>true);
        } else {
            $this->p->e->log($this, array('content' => $token, 'description' => 'Token valid'));
        }

        $this->updatePaymentDetails($this->payment_transaction_id, $answer);

        return array('answer'=>$answer, 'payment'=>$payment);

    }

    public function reverse($id) {
        $this->p->e->log($this, array('content' => array($id), 'description' => 'Starting reverse transaction'));

        $payment = $this->bySystemId($id);
        if (!$payment) {
            $this->p->e->error($this, array('content' => array('payment' => $payment), 'description' => 'payment_not_found'));
            return array('error'=>true);
        }

        $send = array(
            'public_key'=>$this->settings['publicKey'],
            'operation' => array(
                'shop_process_id'=>$this->payment_id,
            )
        );

        $this->p->e->log($this,  array('content' =>  array($send), 'description' => 'Created a token, sending answer.'));
        $send['operation']['token'] = md5($this->settings['privateKey'] . $this->payment_id . "rollback0.00");

        $url = $this->settings['postUrl'] . "/vpos/api/0.3/single_buy/rollback";

        $this->p->e->log($this, array('content' => array($url), 'description' => 'Start curl generation'));

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, trim($url));
        if ($this->settings['postPort']!=false) {
            curl_setopt($ch, CURLOPT_PORT, $this->settings['postPort']);
        }
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($send));
        curl_setopt($ch, CURLOPT_HTTPHEADER,array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLOPT_FILETIME, true);
        curl_setopt($ch, CURLOPT_STDERR, $verbose = fopen('php://temp', 'rw+'));


        $this->p->e->log($this, array('content' => array(), 'description' => 'Send curl'));
        $result = curl_exec($ch);

        $result = json_decode($result, true);


        $this->p->e->log($this, array('content' => $result, 'description' => 'Curl answer'));

        if ($result['status']=="success") {
            $this->p->e->log($this, array('content' => $result, 'description' => 'Transaction reversed'));
            $this->updatePayment($this->payment_transaction_id, 100, false, true);
            return true;
        } else {
            if ($result['messages'][0]['key']=='AlreadyRollbackedError') {
                $this->p->e->log($this, array('content' => $result, 'description' => 'Transaction reversed (retrospec)'));
                $this->updatePayment($this->payment_transaction_id, 100, false, true);
                return true;
            }
            $this->p->e->error($this, array('content' => $result, 'description' => 'bc_error_while_reversing'));
            return array('error'=>true);
        }

    }

    public function createToken($data) {
        $tokenString = $this->settings['privateKey'];
        foreach ($data as $d) {
            $tokenString .= $d;
        }

        return md5($tokenString);
    }


    public function cron() {

        $return = $this->db->executeSQL("SELECT * FROM " . $this->config['table'] . " WHERE status=0 AND `created`<DATE_SUB(NOW(), INTERVAL 10 MINUTE)");

        $data = array();

        foreach ($return as $item) {
            $data[] = $this->reverse($item['system_id']);
        }

        return array('error'=>false);

    }
}