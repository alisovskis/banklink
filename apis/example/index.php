<?php

class Example extends Bank
{

    public $id = 'example';


    public function __construct($parent,$settings) {
        parent::__construct($parent, $settings);
    }

    public function buy($data) {
        $this->p->e->log($this, ['content' => ['data' => $data], 'description' => 'Creating a new payment']);

        $new = $this->createNew($data);
        if (!$new) {
            return ['error'=>true];
        }

        $this->getPayment($this->payment_transaction_id);
    }

    public function confirm() {

    }

}