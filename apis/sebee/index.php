<?php

class Sebee extends Bank
{

    var $id = 'sebee';

    public function buy($data) {

        $this->p->e->log($this, ['content' => ['data' => $data], 'description' => 'Creating a new payment']);

        if ($this->createNew($data)) {

            $this->p->e->log($this, ['content' => [], 'description' => 'Creating a new payment, db entry success.']);

            // Set variables
            $respond['VK_SERVICE'] = '1012';
            $respond['VK_VERSION'] = '008';
            $respond['VK_SND_ID'] = $this->settings['id'];
            $respond['VK_STAMP'] = $this->payment_transaction_id;
            $respond['VK_AMOUNT'] = floatval($data['amount']);
            $respond['VK_CURR'] = $this->p->getConfig('currency');
            $respond['VK_REF'] = $this->createReference();
            $respond['VK_MSG'] = $data['description'];
            $respond['VK_RETURN'] = $this->settings['responseUrl'];
            $respond['VK_CANCEL'] = $this->settings['cancelUrl'];
            $respond['VK_DATETIME'] = date("c", time());
            $respond['VK_ENCODING'] = 'UTF-8';
            $respond['VK_LANG'] = $this->settings['lang'];

            // Prapareing data
            $datastore = str_pad(mb_strlen($respond['VK_SERVICE']), 3, '0', STR_PAD_LEFT) . $respond['VK_SERVICE'];
            $datastore .= str_pad(mb_strlen($respond['VK_VERSION']), 3, '0', STR_PAD_LEFT) . $respond['VK_VERSION'];
            $datastore .= str_pad(mb_strlen($respond['VK_SND_ID']), 3, '0', STR_PAD_LEFT) . $respond['VK_SND_ID'];
            $datastore .= str_pad(mb_strlen($respond['VK_STAMP']), 3, '0', STR_PAD_LEFT) . $respond['VK_STAMP'];
            $datastore .= str_pad(mb_strlen($respond['VK_AMOUNT']), 3, '0', STR_PAD_LEFT) . $respond['VK_AMOUNT'];
            $datastore .= str_pad(mb_strlen($respond['VK_CURR']), 3, '0', STR_PAD_LEFT) . $respond['VK_CURR'];
            $datastore .= str_pad(mb_strlen($respond['VK_REF']), 3, '0', STR_PAD_LEFT) . $respond['VK_REF'];
            $datastore .= str_pad(mb_strlen($respond['VK_MSG']), 3, '0', STR_PAD_LEFT) . $respond['VK_MSG'];
            $datastore .= str_pad(mb_strlen($respond['VK_RETURN']), 3, '0', STR_PAD_LEFT) . $respond['VK_RETURN'];
            $datastore .= str_pad(mb_strlen($respond['VK_CANCEL']), 3, '0', STR_PAD_LEFT) . $respond['VK_CANCEL'];
            $datastore .= str_pad(mb_strlen($respond['VK_DATETIME']), 3, '0', STR_PAD_LEFT) . $respond['VK_DATETIME'];

            // Reading private key

            $this->p->e->log($this, ['content' => [], 'description' => 'Reading public key']);

            $fp = $this->getPublicKey();

            if (!$fp) { return ['error'=>true];}

            $this->p->e->log($this, ['content' => ['data' => $data], 'description' => 'Opening SSL private key']);

            $pkeyid = openssl_get_privatekey('file://' . $this->settings['privateKeyPath'], $this->settings['privateKeyPass']);

            if ($pkeyid!==false) {
                $this->p->e->log($this, ['content' => [], 'description' => 'Opening SSL private key, Success']);

                $this->p->e->log($this, ['content' => ['data'=>$datastore, 'pkey'=>$pkeyid], 'description' => 'Computing signature']);

                openssl_sign($datastore, $signature, $pkeyid);

                $respond['VK_MAC'] = base64_encode($signature);
                $respond['VK_STAMP'] = htmlspecialchars($respond['VK_STAMP']);
                $respond['VK_MSG'] = htmlspecialchars($respond['VK_MSG']);
                $respond['VK_RETURN'] = htmlspecialchars($respond['VK_RETURN']);

                $this->p->e->log($this, ['content' => ['data'=>$datastore, 'pkey'=>$pkeyid], 'description' => 'Payment request created successfuly']);

                return ['error' => false, 'to_url' => $this->settings['postUrl'], 'post' => $respond];

            } else {

                $this->p->e->error($this, ['content' => ['sertificate'=>$fp, 'passphrase'=>$this->settings['privateKeyPass']], 'description' => 'seb_ee_private_key_couldnt_open']);
                return ['error'=>true];
            }
        } else {

            $this->p->e->error($this, ['content' => ['data' => $data], 'description' => 'eebanks_could_not_generate_payment']);

            return ['error'=>true];
        }
    }

    function createReference() {
        $number = $this->payment_id;
        $numberValues = str_split($number);

        $numberValues = array_reverse($numberValues);

        $keys = [7, 3, 1];

        $keyholder = 0;

        $sum = 0;

        foreach ($numberValues as $n) {
            $sum = $sum + ($n * $keys[$keyholder]);

            if ($keyholder == count($keys) - 1) {
                $keyholder = 0;
            } else {
                $keyholder++;
            }
        }

        $nextTen = (ceil($sum/10))*10;

        return $number . ($nextTen - $sum);

    }

    function readReference($number) {
        $realNumber = floor($number/10);

        $check = $number - ($realNumber*10);

        return $realNumber;
    }

    function getPublicKey() {
        if (file_exists($this->settings['publicKeyPath'])) {
            $fp = file_get_contents($this->settings['publicKeyPath'], "r");
            $this->p->e->log($this, ['content' => [], 'description' => 'Reading public key, Success']);
        } else {
            $this->p->e->error($this, ['content' => ['key_path' => $this->settings['publicKeyPath']], 'description' => 'eebanks_reading_public_key_failed']);
            return false;
        }

        return $fp;
    }

    function _verify($mac, $signature) {

        $fp = $this->getPublicKey();

        if (!$fp) { return false;}

        $key = openssl_get_publickey($fp);

        $ok = openssl_verify($mac, $signature, $key);

        openssl_free_key($key);

        return $ok;
    }

    function checkThePurchase($orderInfo) {

        if (!empty($orderInfo)) {
            $status = $this->updatePayment($orderInfo, 1, true);
            if ($status==true) {
                return $this->payment_system_id; // now we will just return true
            } else {
                return false;
            }
        } else {
            return false;
        }

    }

    public function confirm() {

        $this->p->e->log($this, ['content' => ['post' => $_POST, 'get'=>$_GET], 'description' => 'Confirming payment']);

        $return = ['error'=>false, 'id'=>false, 'errorMessage'=>false];

        if(isset($_GET) && !empty($_GET)) {
            $method = $_GET;
        } elseif(isset($_POST) && !empty($_POST)) {
            $method = $_POST;
        }

        if(!isset($method['VK_REC_ID']) || $method['VK_REC_ID'] !=  $this->settings['id']) {
            return false;
        }

        $signature_ok = false;

        $vkRef = $this->readReference($method['VK_REF']);

        $paymentData = $this->getPayment($vkRef, true);

        if (!$paymentData) {
            $this->p->e->error($this, ['content' => ['request' => $vkRef], 'description' => 'payment_not_found']);
            return ['error'=>true];
        }

        if($method['VK_SERVICE'] == '1111') {
            $mac = str_pad(mb_strlen($method['VK_SERVICE']), 3, '0', STR_PAD_LEFT).$method['VK_SERVICE'];
            $mac .= str_pad(mb_strlen($method['VK_VERSION']), 3, '0', STR_PAD_LEFT).$method['VK_VERSION'];
            $mac .= str_pad(mb_strlen($method['VK_SND_ID']), 3, '0', STR_PAD_LEFT).$method['VK_SND_ID'];
            $mac .= str_pad(mb_strlen($method['VK_REC_ID']), 3, '0', STR_PAD_LEFT).$method['VK_REC_ID'];
            $mac .= str_pad(mb_strlen($method['VK_STAMP']), 3, '0', STR_PAD_LEFT).$method['VK_STAMP'];
            $mac .= str_pad(mb_strlen($method['VK_T_NO']), 3, '0', STR_PAD_LEFT).$method['VK_T_NO'];
            $mac .= str_pad(mb_strlen($method['VK_AMOUNT']), 3, '0', STR_PAD_LEFT).$method['VK_AMOUNT'];
            $mac .= str_pad(mb_strlen($method['VK_CURR']), 3, '0', STR_PAD_LEFT).$method['VK_CURR'];
            $mac .= str_pad(mb_strlen($method['VK_REC_ACC']), 3, '0', STR_PAD_LEFT).$method['VK_REC_ACC'];
            $mac .= str_pad(mb_strlen($method['VK_REC_NAME']), 3, '0', STR_PAD_LEFT).$method['VK_REC_NAME'];
            $mac .= str_pad(mb_strlen($method['VK_SND_ACC']), 3, '0', STR_PAD_LEFT).$method['VK_SND_ACC'];
            $mac .= str_pad(mb_strlen($method['VK_SND_NAME']), 3, '0', STR_PAD_LEFT).$method['VK_SND_NAME'];
            $mac .= str_pad(mb_strlen($method['VK_REF']), 3, '0', STR_PAD_LEFT).$method['VK_REF'];
            $mac .= str_pad(mb_strlen($method['VK_MSG']), 3, '0', STR_PAD_LEFT).$method['VK_MSG'];
            $mac .= str_pad(mb_strlen($method['VK_T_DATETIME']), 3, '0', STR_PAD_LEFT).$method['VK_T_DATETIME'];
            $signature_ok = $this->_verify($mac, base64_decode($method['VK_MAC']));
        } else if ($_POST['VK_SERVICE'] == '1911') {
            $mac = str_pad(mb_strlen($_POST['VK_SERVICE']), 3, '0', STR_PAD_LEFT).$_POST['VK_SERVICE'];
            $mac .= str_pad(mb_strlen($_POST['VK_VERSION']), 3, '0', STR_PAD_LEFT).$_POST['VK_VERSION'];
            $mac .= str_pad(mb_strlen($_POST['VK_SND_ID']), 3, '0', STR_PAD_LEFT).$_POST['VK_SND_ID'];
            $mac .= str_pad(mb_strlen($_POST['VK_REC_ID']), 3, '0', STR_PAD_LEFT).$_POST['VK_REC_ID'];
            $mac .= str_pad(mb_strlen($_POST['VK_STAMP']), 3, '0', STR_PAD_LEFT).$_POST['VK_STAMP'];
            $mac .= str_pad(mb_strlen($_POST['VK_REF']), 3, '0', STR_PAD_LEFT).$_POST['VK_REF'];
            $mac .= str_pad(mb_strlen($_POST['VK_MSG']), 3, '0', STR_PAD_LEFT).$_POST['VK_MSG'];
            $signature_ok = $this->_verify($mac, base64_decode($_POST['VK_MAC']));
        } else {
            $method['VK_REF'] = $vkRef;
            $this->updatePayment($method['VK_REF'], 2, true);
            $this->p->e->error($this, ['content' => [], 'description' => 'eebanks_payment_unaccepted']);
            $return['error'] = true;
            return $return;
        }

        $method['VK_REF'] = $vkRef;

        if($signature_ok == false || $signature_ok == 0) {
            $this->updatePayment($method['VK_REF'], 2, true);
            $this->p->e->error($this, ['content' => [], 'description' => 'eebanks_signature_invalid']);
            $return['error'] = true;
            return $return;
        }

        if($method['VK_SERVICE'] == '1111') {
            $return['id'] = $this->payment_system_id;

            /*
            Bank allways sends a GET, but if the client clicks on the buttons to send back to the service provider web page we will receive POST.
            At first, we will receive GET first, but if the client is very fast, or there is some internet communication delay, first will be POST.
            So we need to double check!
            */

            if ($method['VK_AUTO'] == 'Y') {
                // USER PURCHASED
                // Received GET
                /*
                We received a invisible (Client can't see this) GET
                We need to set thet order has paid
                */

                // we need to check the order and if add is well, you need to mark it as paid
                $checkPurchase = $this->checkThePurchase($method['VK_REF']);

                if($checkPurchase !== false) {
                    $return['id']=$checkPurchase;
                    return $return;
                } else {
                    $this->updatePayment($method['VK_REF'], 2, true);
                    $this->p->e->error($this, ['content' => [], 'description' => 'eebanks_errors_updating_payment']);
                    $return['error'] = true;
                }
            } else {
                // USER PURCHASED
                // Received POST
                /*
                This could be only if the client clicked on the button to send back to the service provider web page.
                */

                // we need to check the order and if add is well, you need to mark it as paid
                $checkPurchase = $this->checkThePurchase($method['VK_REF']);

                if($checkPurchase !== false) {
                    $return['id']=$checkPurchase;
                    return $return;
                } else {
                    $this->updatePayment($method['VK_REF'], 2, true);
                    $this->p->e->error($this, ['content' => [], 'description' => 'eebanks_errors_updating_payment']);
                    $return['error'] = true;
                }
            }
        } else {
            $return['id'] = $this->payment_system_id;

            if ($_POST['VK_AUTO'] != 'Y') {
                $this->updatePayment($method['VK_REF'], 2, true);
                $this->p->e->error($this, ['content' => [], 'description' => 'eebanks_status_reveived_fail']);
                $return['error'] = true;
            }
        }

        return $return;
    }
}
/*
     *  1 - Payment wasnt accepted, money wasn't transfered
     *  2 - No confirmation status
     *  3 -
     *  4 - Error with checking signature
     *  5 - Payment not found
     *  6 - Error oppening sertificate
     *  7 - Payment already confirmed
 *
 */