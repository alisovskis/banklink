<?php

require_once 'citadele.class.php';

class Citadel extends Bank
{

    var $id = 'citadele';

    private $url = false;

    function buy($data) {

        $this->p->e->log($this, ['content' => [$data], 'description' => 'Starting payment']);

        $return = ['error'=>false, 'url'=>false, 'post'=>[]];

        $this->p->e->log($this, ['content' => [], 'description' => 'Creating citadele instance']);
        $amai = new CitadeleDigiLink($this->settings, $this->p, $this);
        $this->p->e->log($this, ['content' => [], 'description' => 'Instance created, Creating buy data']);
        $signed = $amai->buy($data);

        if (!$signed) {
            $this->p->e->error($this, ['content' => [], 'description' => 'signature_creation_error']);
            return ['error'=>true];
        } else {
            $this->p->e->log($this, ['content' => [], 'description' => 'Buy data created']);
            $signed['xmldata'] = str_replace(array('&amp;'),array('&amp;amp;'),$signed['xmldata']);

           // var_dump($this->settings['postUrl']);
            $return['to_url'] = $this->settings['postUrl'];
            $return['post']['xmldata'] = $signed['xmldata'];

        }
        $this->p->e->log($this, ['content' => [], 'description' => 'Returning data']);
        return $return;
    }

    function confirm() {

        $this->p->e->log($this, ['content' => ['post'=>$_POST, 'get'=>$_GET], 'description' => 'Creating citadele instance']);
        $amai = new CitadeleDigiLink($this->settings, $this->p, $this);
        $this->p->e->log($this, ['content' => [], 'description' => 'Instance created, Creating buy data']);

        $this->p->e->log($this, ['content' => [], 'description' => 'Verifying received data']);
        $verify = $amai->verify($_POST['xmldata']);

        if ($verify==1) {
            $this->p->e->log($this, ['content' => [], 'description' => 'Data verifiend, creating Array from XML']);
            $result = $this->xmlToArray($_POST['xmldata']);
            $this->p->e->log($this, ['content' => [$result], 'description' => 'Data created']);
        } else {
            $this->p->e->error($this, ['content' => [], 'description' => 'could_not_validate']);
            return ['error'=>true];
        }

           $this->p->e->log($this, ['content' => [], 'description' => 'Getting payment']);
           if (isset($result['Header']['Extension']['Amai']['RequestUID'])) {
               $checkPurchase = $this->getPayment($result['Header']['Extension']['Amai']['RequestUID']);
           } elseif (isset($result['PmtStat']['ExtId'])) {
               $checkPurchase = $this->getPayment($result['PmtStat']['ExtId'], true);
           } else {
               $this->p->e->error($this, ['content' => [], 'description' => 'doesnt_have_any_identifier']);
               return ['error'=>true];
           }

           if (!empty($checkPurchase)) {
               $this->p->e->log($this, ['content' => [], 'description' => 'Payment found, checking status']);
               if ($checkPurchase['status']==0) {
                   $this->p->e->log($this, ['content' => [], 'description' => 'Payment status is OK (0), selecting action.']);

                   if ($result['Header']['Extension']['Amai']['Request']=='PMTRESP') {
                       $this->p->e->log($this, ['content' => [], 'description' => 'Received PMTRESP action, starting it.']);

                       if ($result['Header']['Extension']['Amai']['Code'] == '100') {
                           $this->p->e->log($this, ['content' => [], 'description' => 'Status Ok, must wait for finalizing status!']);
                            return ['error'=>false, 'id'=>$this->payment_system_id, 'status'=>0, 'wait'=>true];
                       } else {
                           if ($result['Header']['Extension']['Amai']['Code'] == '200') {
                               $this->updatePayment($this->payment_transaction_id, 7);
                               $this->p->e->error($this, ['content' => [], 'description' => 'cancelled_transaction']);
                           } else {
                               $this->updatePayment($this->payment_transaction_id, 99);
                               $this->p->e->error($this, ['content' => [], 'description' => 'Error with transaction: ' . $result['Header']['Extension']['Amai']['Message']]);
                           }
                           return ['error' => true];
                       }
                   } else {
                       $this->p->e->log($this, ['content' => [], 'description' => 'Received PMTSTATRESP action, starting it.']);
                       if ($result['PmtStat']['StatCode'] == 'E') {
                           $this->p->e->log($this, ['content' => [], 'description' => 'Status Ok, Payment done, update status data.']);

                           if ($this->updatePayment($this->payment_transaction_id, 1)) {
                               $this->updatePaymentDetails($this->payment_transaction_id, json_encode([
                                   'date'=>$result['PmtStat']['BookDate'],
                                   'accNo'=>$result['PmtStat']['Extension']['AccNo'],
                                   'name'=>$result['PmtStat']['Extension']['Name']
                               ]));
                               $this->p->e->log($this, ['content' => [], 'description' => 'Update successful']);
                               return ['error'=>false, 'id'=>$this->payment_system_id, 'status'=>1, 'wait'=>false];
                           } else {
                               $this->p->e->error($this, ['content' => [], 'description' => 'could_not_update']);
                               return ['error'=>true];
                           }
                       } else {
                           $this->updatePayment($this->payment_transaction_id, 3);
                           $this->updatePaymentDetails($this->payment_transaction_id, json_encode([
                               'accNo'=>$result['PmtStat']['Extension']['AccNo'],
                               'name'=>$result['PmtStat']['Extension']['Name']
                           ]));
                           $this->p->e->error($this, ['content' => [], 'description' => 'declined_transaction']);
                           return ['error'=>true];
                       }
                   }

               } else {
                   $this->p->e->error($this, ['content' => [], 'description' => 'payment_status_already_set']);
                   return ['error'=>true];
               }

           } else {
               $this->p->e->error($this, ['content' => [], 'description' => 'payment_not_found']);
               return ['error'=>true];
           }

    }

}