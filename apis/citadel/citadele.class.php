<?php

/**
 * Citadele DIGI=Link pieslēguma datu apmaiņas nodrošināšana.
 * Pirkuma sagatavošana, parakstīšana, validēšana.
 * 
 * @uses XML/Serializer.php http://pear.php.net/package/XML_Serializer (pear install XML_Serializer)
 * @uses XML/Unserializer.php http://pear.php.net/package/XML_Serializer
 * @uses xmlseclibs.php http://code.google.com/p/xmlseclibs/
 */
class CitadeleDigiLink
{

   private $p = false;
   private $settings = false;
   public $url = false;

   
   /**
    * inicializējam klasi
    *
    * @param bool $test
    */
   function __construct($settings, $parent, $me){

       $this->settings = $settings;
       $this->p = $parent;
       $this->main = $me;

   }

    public function str_baseconvert($str, $frombase=10, $tobase=36) {
        $str = trim($str);
        if (intval($frombase) != 10) {
            $len = strlen($str);
            $q = 0;
            for ($i=0; $i<$len; $i++) {
                $r = base_convert($str[$i], $frombase, 10);
                $q = bcadd(bcmul($q, $frombase), $r);
            }
        }
        else $q = $str;

        if (intval($tobase) != 10) {
            $s = '';
            while (bccomp($q, '0', 0) > 0) {
                $r = intval(bcmod($q, $tobase));
                $s = base_convert($r, 10, $tobase) . $s;
                $q = bcdiv($q, $tobase, 0);
            }
        }
        else $s = $q;

        return $s;
    }

   /**
    * Starts citadele transaction
    * 
    * @param float $amount transaction amount
    * @param string $description transaction description
    * @return array data for request <form> <input type=hidden > element
    */
   function buy($data){


      $new = $this->main->createNew($data);
        if (!$new) {
            return ['error'=>true];
        }

       $this->trans_id = $this->main->payment_transaction_id;
       $this->doc_id = $this->main->payment_id;

       $xml = $this->createPaymentXml($data['amount'], $data['description']);   //header("content-type: text/xml");die($xml); //DEBUG

       //echo $xml;

       $xml = $this->sign($xml);                                //header("content-type: text/xml");die($xml); //DEBUG

       $a = $this->verify($xml);
      if($a){
         return array('xmldata' => str_replace('"', '&quot;', $xml));// prepare data for HTTP POST Form; remove quotes
      }

      //var_dump($a);
      //var_dump(openssl_error_string());
      
      /**
       * @todo handle error
       */
      return false;
   }

   /**
    * Create clean payment xml. Must sign after.
    *
    * @param float $amount
    * @param string $description optional
    * @return xml
    */
   function createPaymentXml($amount, $description = null, $language = 'LAT'){

      $digilink_params = array(
          'Header' => array(
             'Timestamp' => substr(date('YmdHisu'), 0, 17), //Sagatavošanas datums un laiks formātā YYYYMMDDHHMMSSsss.
             'From'      => $this->settings['id'],                   //Arējai sistēmai piešķirtais kontrakta identifikators.
             'Extension' => array(
                'Amai' => array(
                   '_attributes'  => array(
                       'xmlns'              => 'http://online.citadele.lv/XMLSchemas/amai/',
                       'xmlns:xsi'          => 'http://www.w3.org/2001/XMLSchema-instance',
                       'xsi:schemaLocation' => 'http://online.citadele.lv/XMLSchemas/amai/ http://online.citadele.lv/XMLSchemas/amai/amai.xsd'
                    ),
                   'Request'       => 'PMTREQ',          //Pieprasījums. Vērtība: PMTREQ
                   'RequestUID'    => $this->trans_id,   //Pieprasījuma unikāls identifikators.
                   'Version'       => '3.0',             //Versija. Vērtība=1.0
                   'Language'      =>  $this->main->settings['langList'][$this->main->settings['lang']],         //Valoda. Iespējamas vērtības: LV, RU, EN.
                   'ReturnURL'     => $this->main->settings['responseUrl'], //Atgriešanas URL. Tiek izmantots, lai atgrieztos uz Ārējās sistēmas portāla lapu.
                   'SignatureData' => ''                 //Tiks aizpildīts parakstot
                )
             )
          ),
          'PaymentRequest' => array(                     //Maksājuma dati
             'ExtId'     => $this->doc_id,             //Maksājuma unikāls identifikators Ārējā sistēmā.
             'DocNo'     => $this->doc_id,             //Maksājuma numurs.
             'TaxPmtFlg' => 'N',                         //Pazīme par to vai maksājums ir vai nav nodokļu maksājums. Vērtība=N
             'Ccy'       => $this->p->getConfig('currency'),             //Maksājuma valūta. LVL | EUR
             'PmtInfo'   => substr($description, 0, 140),                //Maksājuma detaļas (informācija saņēmējam). Obligāts lauks. Atļautie simboli: A-Z| a-z| Ā-Ž | ā-ž |0-9|()+. /:-’,
             'BenSet' => array(                          //Maksājuma saņēmēja dati.
                'Priority'   => 'N',                     //Maksājuma prioritāte. Vērtība=N
                'Comm'       => 'OUR',                   //Komisijas tips. Vērtība=OUR
                'Amt'        => sprintf('%0.2f', str_replace(',', '.', $amount)), //Maksājuma summa. <= 50 000
                'BenAccNo'   => $this->main->settings['accountNumber'],        //Saņēmēja konta numurs. Latvijas IBAN formātā
                'BenName'    =>  $this->p->getConfig('receiver'),          //Saņēmēja nosaukums. Atļautie simboli: A-Z| a-z| Ā-Ž | ā-ž |0-9|()+. /:-’,
                'BenLegalId' =>  $this->main->settings['legalId'],      //Saņēmēja reģistrācijas numurs vai nodokļu maksātāja kods. Atļautie simboli: A-Z| a-z| 0-9|()+. /:-’,”& Maksimālais garums: 13
                'BenCountry' =>  $this->main->settings['legalCountry']       //Valsts kods, kurā reģistrēts saņēmējs. Vērtība=LV
              )
           )
      );

      // Instantiate the serializer
      $serializer = new XML_Serializer(array(
         'addDecl'         => true,
         'encoding'        => 'UTF-8',
         'rootName'        => 'FIDAVISTA',
         'attributesArray' => '_attributes',
          //XML_SERIALIZER_OPTION_INDENT        => '    ',//for debugging
         'rootAttributes'  => array(
            'xmlns'              => 'http://ivis.eps.gov.lv/XMLSchemas/100017/fidavista/v1-1',
            'xmlns:xsi'          => 'http://www.w3.org/2001/XMLSchema-instance',
            'xsi:schemaLocation' => 'http://ivis.eps.gov.lv/XMLSchemas/100017/fidavista/v1-1 http://ivis.eps.gov.lv/XMLSchemas/100017/fidavista/v1-1/fidavista.xsd'
         )
      ));

      $serializer->serialize($digilink_params);
      $xml = $serializer->getSerializedData();
      
      return str_replace(array("\n", "\t", "\r", "\r\n"), '', trim($xml)); // clean xml
   }

   /**
    * Sign xml data and return signed xml
    *
    * @param xml $xml
    * @return xml
    */
   function sign($xml){
      $doc = new DOMDocument();
      $doc->loadXML($xml);

      $objDSig = new XMLSecurityDSig();

      $objDSig->setCanonicalMethod(XMLSecurityDSig::EXC_C14N);
      //$objDSig->setCanonicalMethod(XMLSecurityDSig::C14N);//vajag būt, bet ar šo nesanāk validēt

      $objDSig->addReference($doc, XMLSecurityDSig::SHA1, array('http://www.w3.org/2000/09/xmldsig#enveloped-signature'), array('force_uri' => true));
      $objKey = new XMLSecurityKey(XMLSecurityKey::RSA_SHA1, array('type'=>'private', 'passphrase'=>$this->settings['privateKeyPass']));
      $objKey->loadKey($this->settings['privateKeyPath'], TRUE);

      $objDSig->sign($objKey);
      $objDSig->add509Cert(file_get_contents($this->settings['publicKeyPath']));
      $appendSignatureTo = $doc->getElementsByTagName('SignatureData')->item(0);
      $objDSig->appendSignature($appendSignatureTo);

      return $doc->saveXML();
   }

   /**
    * Verify xml request
    *
    * @param xml $xml
    * @return bool
    */
   function verify($xml){

      $doc = new DOMDocument();
      $doc->loadXML($xml);
      $objXMLSecDSig = new XMLSecurityDSig();

      if(!$objDSig = $objXMLSecDSig->locateSignature($doc)){
         throw new Exception("Cannot locate Signature Node");
      }
      $objXMLSecDSig->canonicalizeSignedInfo();
      $objXMLSecDSig->idKeys = array('wsu:Id');
      $objXMLSecDSig->idNS   = array('wsu'=>'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd');

      if(!$retVal = $objXMLSecDSig->validateReference()){
         throw new Exception("Reference Validation Failed");
      }

      if(!$objKey = $objXMLSecDSig->locateKey()){
         throw new Exception("We have no idea about the key");
      }
      $key = NULL;

      $objKeyInfo = XMLSecEnc::staticLocateKeyInfo($objKey, $objDSig);

      // ja nav norādīta atslēga, tad norāda to. šajā gadījumā nav vajadzīgs, jo tiek norādīta xml`ā
      //if(!$objKeyInfo->key && empty($key)){
      //   $objKey->loadKey($this->public_cert, TRUE);
      //}


       return $objXMLSecDSig->verify($objKey);
   }


   /**
    * Translates xml response into mixed array
    *
    * @param xml string $response
    * @return array
    */
   public function xmlToArray($response){
      $unserializer = new XML_Unserializer();
      $status = $unserializer->unserialize($response);
      error_reporting(E_ERROR);
      if(PEAR::isError($status)){
          error_reporting(E_ALL);
         //$status->getMessage(); //error message
         return false;
      }
       error_reporting(E_ALL);
      return $unserializer->getUnserializedData();
   }
}

?>