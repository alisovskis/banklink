<?

include_once __DIR__ . "/banklink/Banklink.php";

class Example
{
    public $bl = false;

    function __construct() {
        $this->bl = new Banklink();
    }

    function buy()
    {
        $this->bl->setBank('firstdata', ['enabled' => true]);

        $result = $this->bl->buy('firstdata', ['amount' => 320, 'user_id' => false, 'description' => 'Order number: 123', 'id' => rand(10000, 99999)]);

        if ($result['error']==false) {
            echo '<form action="' . $result['to_url'] . '">';

            foreach ($result['post'] as $key => $post) {
                echo '<input type=hidden value="' . $post . '" name="' . $key . '"/>';
            }

            echo '<button type="submit"></button>';
            echo '</form>';
        } else {
            var_dump($result);
        }
    }

    function confirm () {
        $result = $this->bl->confirm('firstdata');

        var_dump($result);
    }

    function error () { //fail link
        $result = $this->bl->error('firstdata');

        var_dump($result);
    }

    function cron() {
        $result = $this->bl->cron();

        var_dump($result);
    }

}