<?php

/**
 * Citadele DIGI=Link pieslēguma datu apmaiņas nodrošināšana.
 * Pirkuma sagatavošana, parakstīšana, validēšana.
 * 
 * @uses XML/Serializer.php http://pear.php.net/package/XML_Serializer (pear install XML_Serializer)
 * @uses XML/Unserializer.php http://pear.php.net/package/XML_Serializer
 * @uses xmlseclibs.php http://code.google.com/p/xmlseclibs/
 */
class CitadeleDigiLink
{
   /**
    * Arējai sistēmai piešķirtais kontrakta identifikators.
    */
   const c_from        = 'XXXX';

   /**
    * Saņēmēja konta numurs. Latvijas IBAN formātā
    */
   const ben_acc_no    = 'LV83PARXXXXXXXXXXXXXX';

   /**
    * Saņēmēja nosaukums. Atļautie simboli: A-Z| a-z| Ā-Ž | ā-ž |0-9|()+. /:-’,
    */
   const ben_name      = 'XXXXXXXXX SIA';

   /**
    * Saņēmēja reģistrācijas numurs vai nodokļu maksātāja kods. Atļautie simboli: A-Z| a-z| 0-9|()+. /:-’,”& Maksimālais garums: 13
    */
   const ben_legal_id  = 'XXXXXXXXXXX';

   /**
    * Valsts kods, kurā reģistrēts saņēmējs. Vērtība=LV
    */
   const ben_country   = 'LV';

   /**
    * Produkcijas vides url
    */
   const production_url= 'https://online.citadele.lv/amai/start.htm';

   /**
    * Testēšanas url
    */
   const testing_url   = 'https://astra.parex.lv/amai/start.htm';

   /**
    * Valūta
    *
    * @var string
    */
   private $currency = 'LVL';

   /**
    * Atbildes url
    *
    * @var string
    */
   private $return_url = 'http://myreturnurl.com/';

   /**
    * ceļš līdz privātajai atslēgai
    */
   private $private_key = './my_citadele-key.pem';

   /**
    * ceļš līdz publiskajai atslēgai / sertifikātam
    */
   private $public_cert  = './my_citadele-cert.pem';

   /**
    * citadele sertifikāts
    * netiek lietots, jo tiek padots kopā ar datiem
    */
   private $ssl_public_cert = 'DIGILINK_CITADELE_TEST.cer';

   /**
    * citadele url uz kuru tiek sūtīts pieprasījums
    */
   public $url = '';

   
   /**
    * inicializējam klasi
    *
    * @param bool $test
    */
   function __construct($test = true){
      $this->url = $test ? self::testing_url : self::production_url;
   }

   /**
    * Starts citadele transaction
    * 
    * @param float $amount transaction amount
    * @param string $description transaction description
    * @return array data for request <form> <input type=hidden > element
    */
   function start($amount, $description = null){
      /**
       * @todo must create something more uniqe. max length = 10
       */
      $this->trans_id = substr(uniqid(), 0, 10);

      $xml = $this->createPaymentXml($amount, $description);   //header("content-type: text/xml");die($xml); //DEBUG
      $xml = $this->sign($xml);                                //header("content-type: text/xml");die($xml); //DEBUG

      $this->log("startTransaction: TransID=".$this->trans_id."; Amount=$amount; UserAgent=".$_SERVER['HTTP_USER_AGENT']);
      if($this->verify($xml)){
         return array('xmldata' => str_replace('"', '&quot;', $xml));// prepare data for HTTP POST Form; remove quotes
      }
      
      /**
       * @todo handle error
       */
      die('error signing data');
   }

   /**
    * Create clean payment xml. Must sign after.
    *
    * @param float $amount
    * @param string $description optional
    * @return xml
    */
   function createPaymentXml($amount, $description = null, $language = 'LV'){
      $digilink_params = array(
          'Header' => array(
             'Timestamp' => substr(date('YmdHisu'), 0, 17), //Sagatavošanas datums un laiks formātā YYYYMMDDHHMMSSsss.
             'From'      => self::c_from,                   //Arējai sistēmai piešķirtais kontrakta identifikators.
             'Extension' => array(
                'Amai' => array(
                   '_attributes'  => array(
                       'xmlns'              => 'http://online.citadele.lv/XMLSchemas/amai/',
                       'xmlns:xsi'          => 'http://www.w3.org/2001/XMLSchema-instance',
                       'xsi:schemaLocation' => 'http://online.citadele.lv/XMLSchemas/amai/ http://online.citadele.lv/XMLSchemas/amai/amai.xsd'
                    ),
                   'Request'       => 'PMTREQ',          //Pieprasījums. Vērtība: PMTREQ
                   'RequestUID'    => $this->trans_id,   //Pieprasījuma unikāls identifikators.
                   'Version'       => '1.0',             //Versija. Vērtība=1.0
                   'Language'      => $language,         //Valoda. Iespējamas vērtības: LV, RU, EN.
                   'ReturnURL'     => $this->return_url, //Atgriešanas URL. Tiek izmantots, lai atgrieztos uz Ārējās sistēmas portāla lapu.
                   'SignatureData' => ''                 //Tiks aizpildīts parakstot
                )
             )
          ),
          'PaymentRequest' => array(                     //Maksājuma dati
             'ExtId'     => $this->trans_id,             //Maksājuma unikāls identifikators Ārējā sistēmā.
             'DocNo'     => $this->trans_id,             //Maksājuma numurs.
             'TaxPmtFlg' => 'N',                         //Pazīme par to vai maksājums ir vai nav nodokļu maksājums. Vērtība=N
             'Ccy'       => $this->currency,             //Maksājuma valūta. LVL | EUR
             'PmtInfo'   => $description,                //Maksājuma detaļas (informācija saņēmējam). Obligāts lauks. Atļautie simboli: A-Z| a-z| Ā-Ž | ā-ž |0-9|()+. /:-’,
             'BenSet' => array(                          //Maksājuma saņēmēja dati.
                'Priority'   => 'N',                     //Maksājuma prioritāte. Vērtība=N
                'Comm'       => 'OUR',                   //Komisijas tips. Vērtība=OUR
                'Amt'        => sprintf('%0.2f', str_replace(',', '.', $amount)), //Maksājuma summa. <= 50 000
                'BenAccNo'   => self::ben_acc_no,        //Saņēmēja konta numurs. Latvijas IBAN formātā
                'BenName'    => self::ben_name,          //Saņēmēja nosaukums. Atļautie simboli: A-Z| a-z| Ā-Ž | ā-ž |0-9|()+. /:-’,
                'BenLegalId' => self::ben_legal_id,      //Saņēmēja reģistrācijas numurs vai nodokļu maksātāja kods. Atļautie simboli: A-Z| a-z| 0-9|()+. /:-’,”& Maksimālais garums: 13
                'BenCountry' => self::ben_country        //Valsts kods, kurā reģistrēts saņēmējs. Vērtība=LV
              )
           )
      );

      // Instantiate the serializer
      $serializer = &new XML_Serializer(array(
         'addDecl'         => true,
         'encoding'        => 'UTF-8',
         'rootName'        => 'FIDAVISTA',
         'attributesArray' => '_attributes',
          //XML_SERIALIZER_OPTION_INDENT        => '    ',//for debugging
         'rootAttributes'  => array(
            'xmlns'              => 'http://ivis.eps.gov.lv/XMLSchemas/100017/fidavista/v1-1',
            'xmlns:xsi'          => 'http://www.w3.org/2001/XMLSchema-instance',
            'xsi:schemaLocation' => 'http://ivis.eps.gov.lv/XMLSchemas/100017/fidavista/v1-1 http://ivis.eps.gov.lv/XMLSchemas/100017/fidavista/v1-1/fidavista.xsd'
         )
      ));

      $serializer->serialize($digilink_params);
      $xml = $serializer->getSerializedData();
      
      return str_replace(array("\n", "\t", "\r", "\r\n"), '', trim($xml)); // clean xml
   }

   /**
    * Sign xml data and return signed xml
    *
    * @param xml $xml
    * @return xml
    */
   function sign($xml){
      $doc = new DOMDocument();
      $doc->loadXML($xml);

      $objDSig = new XMLSecurityDSig();

      $objDSig->setCanonicalMethod(XMLSecurityDSig::EXC_C14N);
      //$objDSig->setCanonicalMethod(XMLSecurityDSig::C14N);//vajag būt, bet ar šo nesanāk validēt

      $objDSig->addReference($doc, XMLSecurityDSig::SHA1, array('http://www.w3.org/2000/09/xmldsig#enveloped-signature'), array('force_uri' => true));
      $objKey = new XMLSecurityKey(XMLSecurityKey::RSA_SHA1, array('type'=>'private'));
      $objKey->loadKey($this->private_key, TRUE);

      $objDSig->sign($objKey);
      $objDSig->add509Cert(file_get_contents($this->public_cert));
      $appendSignatureTo = $doc->getElementsByTagName('SignatureData')->item(0);
      $objDSig->appendSignature($appendSignatureTo);

      return $doc->saveXML();
   }

   /**
    * Verify xml request
    *
    * @param xml $xml
    * @return bool
    */
   function verify($xml){
      $doc = new DOMDocument();
      $doc->loadXML($xml);
      $objXMLSecDSig = new XMLSecurityDSig();

      if(!$objDSig = $objXMLSecDSig->locateSignature($doc)){
         throw new Exception("Cannot locate Signature Node");
      }
      $objXMLSecDSig->canonicalizeSignedInfo();
      $objXMLSecDSig->idKeys = array('wsu:Id');
      $objXMLSecDSig->idNS   = array('wsu'=>'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd');

      if(!$retVal = $objXMLSecDSig->validateReference()){
         throw new Exception("Reference Validation Failed");
      }

      if(!$objKey = $objXMLSecDSig->locateKey()){
         throw new Exception("We have no idea about the key");
      }
      $key = NULL;

      $objKeyInfo = XMLSecEnc::staticLocateKeyInfo($objKey, $objDSig);

      // ja nav norādīta atslēga, tad norāda to. šajā gadījumā nav vajadzīgs, jo tiek norādīta xml`ā
      //if(!$objKeyInfo->key && empty($key)){
      //   $objKey->loadKey($this->public_cert, TRUE);
      //}

      return $objXMLSecDSig->verify($objKey);
   }


   /**
    * Translates xml response into mixed array
    *
    * @param xml string $response
    * @return array
    */
   public function xmlToArray($response){
      $unserializer = &new XML_Unserializer();
      $status = $unserializer->unserialize($response);
      if(PEAR::isError($status)){
         //$status->getMessage(); //error message
         return false;
      }
      return $unserializer->getUnserializedData();
   }

   /**
    * Log text to ...
    *
    * @todo make logging
    * @param string $text
    */
   function log($text) {
      //log text
   }
}

?>