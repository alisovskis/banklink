<?php
require_once 'citadele.class.php';

$amai = new CitadeleDigiLink();
$signed = $amai->start("12345", "54321");

/*
 * Important things to note:
 * - Initially none of xml special characters must be escaped, unless intended
 * - During canonicalization special characters will be escaped automatically
 * - Outputting to HTML requires additional level of escaping
 * 
 * XML special character list
 * "   &quot;
 * '   &apos;
 * <   &lt;
 * >   &gt;
 * &   &amp;
 * 
 * Usually "&" are present in ReturnURL
 * if any of other 4 special characters may present in xml contents make sure they are additionally escaped here
 */
$signed['xmldata'] = str_replace(array('&amp;'),array('&amp;amp;'),$signed['xmldata']);

echo '<form action="http://link.to.amai.lv" method="POST">';
echo '<input type="hidden" name="xmldata" value="'.$signed['xmldata'].'" />'; 
echo '<input type="submit" name="submit" value="submit" />';
echo '</form>';

?>