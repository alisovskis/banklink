<?php

$config = array(
    'tablePrefix'=>'bank_',
    'receiver'=>'My company',
    'responseUrl'=>'/?bank=#bank#',
    'failUrl'=>'/?bank=#bank#&error',
    'cancelUrl'=>'/?bank=#bank#&cancel',
    'currency'=>'EUR',
    'log_table'=>'#prefix#banklink_log',
    'debug_table'=>'#prefix#banklink_log',
    'error_table'=>'#prefix#banklink_log',
    'log'=>true,
    'debug'=>true,
    'table'=>'#prefix#banklink_payment',
    'startForcedRequests'=>15,
    'failedRequest'=>30,
    'framework'=>'yii',
    'production'=>false,
    'libraries'=>
        array(
            'example'=>array(
                'enabled'=>false,
                'transactionKey'=>'an', //alphanumerical = an; numerical - n;
                'transactionKeyLen'=>10, //key lenght for transaction key. If not set - used default 10 for an and 4 for n
                'id'=>'', //Bank given id for company
                'lang'=>'LAT', //Default language for interface (after transforms for bank specifications
                'langList'=>array('LAT'=>1, 'RUS'=>2), //mapping for langauges local to langauges for bank. If not set - not used.
                'test'=>array(
                    'id'=>'testId'//overwrites original ID if 'production' = false
                )

            ),
            'seb'=>array(
                'enabled'=>false,
                'privateKeyPath'=>'',
                'publicKeyPath'=>'',
                'test' => array(
                    'postUrl'=>'https://ib-test.unibanka.lv/ipc/epakindex.jsp',
                ),
                'postUrl'=>'https://ibanka.seb.lv/ipc/epakindex.jsp',
                'lang'=>'LAT',
                'id'=>'', //app id
                'privateKeyPass'=>'',
                'ibFeedback'=>'',
                'transactionKey'=>'an'
            ),
            'swedbank'=>array(
                'enabled'=>false,
                'postUrl'=>'https://ib.swedbank.lv/banklink/',
                'id'=>'', //app id
                'privateKeyPath' => '',
                'publicKeyPath' => '',
                'privateKeyPass'=>'',
                'lang'=>'LAT',
                'privateKeyPass'=>'',
                'transactionKey'=>'an'
            ),
            'sebee'=>[
                'enabled'=>false,
                'postUrl'=>'https://www.seb.ee/cgi-bin/ipank/ipank.p',
                'test' => [
                    'postUrl'=>'https://www.seb.ee/cgi-bin/dv.sh/ipank.r',
                    'id'=>'testvpos',
                ],
                'id'=>'', //app id
                'privateKeyPath' => '',
                'publicKeyPath' => '',
                'privateKeyPass'=>'',
                'lang'=>'EST',
                'privateKeyPass'=>'',
                'transactionKey'=>'an'
            ],
            'swedbankee'=>[
                'enabled'=>false,
                'postUrl'=>'https://www.swedbank.ee/banklink',
                'id'=>'', //app id
                'privateKeyPath' => '',
                'publicKeyPath' => '',
                'privateKeyPass'=>'',
                'lang'=>'EST',
                'privateKeyPass'=>'',
                'transactionKey'=>'an'
            ],
            'citadel'=>array(
                'enabled'=>false,
                'langList'=>array('LAT'=>'LV', 'RUS'=>'RU', 'ENG'=>'EN'),
                'privateKeyPath'=>'',
                'publicKeyPath'=>'',
                'privateKeyPass'=>'',
                'SSLpublicKeyPath'=>'',
                'postUrl'=>'https://online.citadele.lv/amai/start.htm',
                'lang'=>'LAT',
                'legalCountry'=>'',
                'legalId'=>'', //receiver reg. nr.
                'legalName'=>'',
                'accountNumber'=>'',
                'id'=>'', //c_from
                'transactionKey'=>'an',
                'test'=>array(
                    'postUrl'=>'https://astra.parex.lv/amai/start.htm',
                )
            ),
            'firstdata'=>array(
                'enabled'=>false,
                'serverUrl'=>'https://secureshop.firstdata.lv:8443/ecomm/MerchantHandler',
                'postUrl'=>'https://secureshop.firstdata.lv/ecomm/ClientHandler',
                'privateKeyPath'=>'',
                'privateKeyPass'=>'',
                'currency'=>'978',
                'lang'=>'LAT',
                'ip'=>false,
                'langList'=>array('LAT'=>'lv', 'RUS'=>'ru', 'ENG'=>'en'),
                'transactionKey'=>'an',
                'type'=>'SMS',
                'printForTest'=>false,
                'test'=>array(
                    'serverUrl'=>'https://secureshop-test.firstdata.lv:8443/ecomm/MerchantHandler',
                    'postUrl'=>'https://secureshop-test.firstdata.lv/ecomm/ClientHandler',
                )
            ),
            'nordea'=>array(
                'enabled'=>false,
                'postUrl'=>'https://netbank.nordea.com/pnbepay/epayn.jsp',
                'postUrlQuery'=>'https://netbank.nordea.com/pnbepay/query.jsp',
                'lang'=>'LAT',
                'langList'=>array('LAT'=>6, 'ENG'=>3, 'EST'=>4, 'LIT'=>7),
                'paymentVersion' => '0003',
                'id'=>'',
                'mac'=>'',
                'refCalc'=>array(7, 3, 1),
                'encodeUse'=>'MD5',
                'macKeyVersion'=>'0001',
                'transactionKey'=>'n',
                'test'=>array(
                    'postUrl'=>'https://netbank.nordea.com/pnbepaytest/epayn.jsp',
                    'postUrlQuery'=>'https://netbank.nordea.com/pnbepaytest/query.jsp',
                    'encodeUse'=>'MD5',
                    'id'=>'12345678', //mac
                    'mac'=>'LEHTI',
                )
            ),
            'norvik'=>array(
                'enabled'=>false,
                'receiver'=>'',
                'id'=>'',
                'type'=>'',
                'transactionKey'=>'n',
                'currencyCode'=>11, //j�p�rjaut� priek� EUR!!!
                'currency'=>'EUR',
                'legalName'=>'',
                'legalAccount'=>'',
                'legalId'=>'',
                'legalCountry'=>'LV',
                'lang'=>'LAT',
                'legalCountryName'=>'',
                'postUrl'=>'https://www.e-norvik.lv/banklink.cfm',
                'privateKeyPath'=>'',
                'publicKeyPath'=>'',
                'privateKeyPass'=>'',
                'test'=>array(
                    'postUrl'=>'http://ebtest.norvik.eu/banklink.cfm'
                )
            ),
            'bancard'=>array(
                'enabled'=>true,
                'postUrl'=>'https://vpos.infonet.com.py',
                'postPort'=>false,
                'test'=>array(
                    'postUrl'=>'https://vpos.infonet.com.py:8888',
                    'postPort'=>false,
                ),
                'privateKey'=>'',
                'publicKey'=>'',
                'transactionKey'=>'n',
                'transactionKeyLen'=>4,

            )
    )
);